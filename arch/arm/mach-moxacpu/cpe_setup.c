/*
 * History:
 * Date		Author			Comment
 * 05-12-2005	Victor Yu.		Create it.
 */
#include <linux/init.h>
#include <linux/device.h>
#include <linux/serial.h>
#include <linux/tty.h>
#include <linux/serial_core.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>

#include <asm/elf.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/arch/cpe/cpe.h>
#include <asm/arch/irq.h>
#include <asm/arch/hardware.h>
#include <asm/sizes.h>
#include <asm/mach/map.h>

/*
 * Following is used Faraday demo board
 * Physical     Logical
 * 98100000     f9810000    Power Management
 * 98200000     f9820000    UART 1
 * 98300000     f9830000    UART 2
 * 98400000     f9840000    Timer 1/2
 * 98800000     f9880000    INTC
 * 96700000     f9670000    MAC 
 * 98700000     f9870000    GPIO
 * 98b00000     f98b0000    SSP1
 * 90400000     f9040000    AHB
 * 90100000     f9010000    AHB
 * 80400000     f0400000    Flash
 * 90c00000     90c00000    PCI base/IO
 * b0000000     b0000000    PCI memory resource
 * fb080000     b0800000    A321 Interrupt Controller
 *
 * Following is used Moxa CPU demo board
 * Physical     Logical
 * 90100000	f9010000	AHB controller
 * 90200000	f9020000	SMC
 * 90300000	f9030000	SDRAM controller
 * 90400000	f9040000	DMA
 * 90500000	f9050000	APB bridge
 * 90900000	f9090000	MAC #1
 * 90a00000	f90a0000	USB 2.0 host
 * 90b00000	f90b0000	USB 2.0 device
 * 90c00000	f90c0000	PCI bridge
 * 90f00000	f90f0000	DES/3DES/AES encryptor
 * 92000000	f9200000	MAC #2
 * 92300000	f9230000	EBI
 * 98100000	f9810000	PMU (power management)
 * 98200000	f9820000	UART (port 1 - 6), embedded on CPU
 * 98400000	f9840000	timer #1 & #2
 * 98500000	f9850000	watchdog timer
 * 98600000	f9860000	RTC, embedded on CPU
 * 98700000	f9870000	GPIO
 * 98800000	f9880000	INTC (interrupt controller)
 * 98b00000	f98b0000	SPI
 * 98e00000	f98e0000	SD controller
 * 99400000	f9940000	AC97
 * a0000000	a0000000	PCI memory
 */

static struct map_desc cpe_io_desc[] __initdata = {
#ifdef CONFIG_ARCH_CPE
 { CPE_PMU_VA_BASE,     CPE_PMU_BASE,       SZ_4K, MT_DEVICE},
 { CPE_UART1_VA_BASE,   CPE_UART1_BASE,     SZ_4K, MT_DEVICE}, 
 { CPE_UART2_VA_BASE,   CPE_UART2_BASE,     SZ_4K, MT_DEVICE},
 { CPE_TIMER1_VA_BASE,  CPE_TIMER1_BASE,    SZ_4K, MT_DEVICE},
 { CPE_IC_VA_BASE,      CPE_IC_BASE,        SZ_4K, MT_DEVICE},
 { CPE_FTMAC_VA_BASE,   CPE_FTMAC_BASE,     SZ_4K, MT_DEVICE},
 { CPE_GPIO_VA_BASE,    CPE_GPIO_BASE,      SZ_4K, MT_DEVICE},
 { CPE_SSP1_VA_BASE,    CPE_SSP1_BASE,      SZ_4K, MT_DEVICE},
 { CPE_AHBDMA_VA_BASE,  CPE_AHBDMA_BASE,    SZ_4K, MT_DEVICE},
 { CPE_AHB_VA_BASE,     CPE_AHB_BASE,       SZ_4K, MT_DEVICE},
 { CPE_FLASH_VA_BASE,   CPE_FLASH_BASE,     CPE_FLASH_SZ,  MT_DEVICE}, 
 { CPE_PCI_VA_MEM,      CPE_PCI_MEM,        PCI_MEM_SIZE,  MT_DEVICE},
 { CPE_PCI_VA_BASE,     CPE_PCI_BASE,       PCI_MEM_SIZE,  MT_DEVICE},  
 { CPE_A321_IC_VA_BASE, CPE_A321_IC_BASE,   SZ_4K, MT_DEVICE},
 { CPE_HOST20_VA_BASE,  CPE_HOST20_BASE,    SZ_4K, MT_DEVICE},
 { CPE_USBDEV_VA_BASE,  CPE_USBDEV_BASE,    SZ_4K, MT_DEVICE},
 { IO_ADDRESS(0x90E00000),    0x90E00000, SZ_4K, MT_DEVICE},// for HW crypto engine
 { IO_ADDRESS(0xB0900000),    0xB0900000, SZ_4K, MT_DEVICE},// for embedded UART
 { CPE_SD_VA_BASE,      CPE_SD_BASE,        SZ_4K, MT_DEVICE},   //SD
#endif	// CONFIG_ARCH_CPE

#ifdef CONFIG_ARCH_MOXACPU
	{CPE_AHB_VA_BASE,	CPE_AHB_BASE,		SZ_4K, MT_DEVICE},
	{CPE_AHBDMA_VA_BASE,	CPE_AHBDMA_BASE,	SZ_4K, MT_DEVICE},
	{CPE_APBDMA_VA_BASE,	CPE_APBDMA_BASE,	SZ_4K, MT_DEVICE},
	{CPE_PMU_VA_BASE,	CPE_PMU_BASE,		SZ_4K, MT_DEVICE},
	{CPE_TIMER_VA_BASE,	CPE_TIMER_BASE,		SZ_4K, MT_DEVICE},
	{CPE_GPIO_VA_BASE,	CPE_GPIO_BASE,		SZ_4K, MT_DEVICE},
	{CPE_IC_VA_BASE,	CPE_IC_BASE,		SZ_4K, MT_DEVICE},
	{CPE_SD_VA_BASE,	CPE_SD_BASE,		SZ_4K, MT_DEVICE},
	{CPE_PCI_VA_BASE,	CPE_PCI_BASE,		SZ_64K, MT_DEVICE},  
 	{PCI_MEM_VA_BASE,	CPE_PCI_MEM,		PCI_MEM_SIZE, MT_DEVICE},
	{CPE_FTMAC_VA_BASE,	CPE_FTMAC_BASE,		SZ_4K, MT_DEVICE},
	{CPE_FTMAC2_VA_BASE,	CPE_FTMAC2_BASE,	SZ_4K, MT_DEVICE},
	{CPE_USBDEV_VA_BASE,	CPE_USBDEV_BASE,	SZ_4K, MT_DEVICE},
	{CPE_UART_VA_BASE,	CPE_UART_BASE,		SZ_4K, MT_DEVICE},
	{CPE_SPI_VA_BASE,	CPE_SPI_BASE,		SZ_4K, MT_DEVICE},
	{CPE_USBHOST_VA_BASE,	CPE_USBHOST_BASE,	SZ_4K, MT_DEVICE},	
	{CPE_AES_DES_VA_BASE,	CPE_AES_DES_BASE, 	SZ_4K, MT_DEVICE},
	{CPE_AC97_VA_BASE,	CPE_AC97_BASE,		SZ_4K, MT_DEVICE},
	{CPE_EBI_VA_BASE,	CPE_EBI_BASE,		SZ_4K, MT_DEVICE},
	{CPE_WATCHDOG_VA_BASE,	CPE_WATCHDOG_BASE,	SZ_4K, MT_DEVICE},
	{CPE_RTC_VA_BASE,	CPE_RTC_BASE,		SZ_4K, MT_DEVICE},
	{CPE_FLASH_VA_BASE,	CPE_FLASH_BASE,		CPE_FLASH_SZ, MT_DEVICE}, 
/*
#if defined CONFIG_ARCH_W311
	{CPE_WLAN_LED_REG_VA_BASE, CPE_WLAN_LED_REG_BASE, SZ_4K, MT_DEVICE}, 

#endif
*/
#if defined(CONFIG_ARCH_IA241_32128)
	{CPE_FLASH_VA_BASE2,	CPE_FLASH_BASE2,	CPE_FLASH_SZ, MT_DEVICE}, 
#endif	// CONFIG_ARCH_IA241_32128
#endif	// CONFIG_ARCH_MOXACPU
};

#if 1	// add by Victor Yu. 06-02-2005
static struct uart_port cpe_serial_ports[] = {
	{
		.iobase		= CPE_UART1_VA_BASE,
		.mapbase	= CPE_UART1_BASE,
#ifdef CONFIG_ARCH_CPE
		.irq		= IRQ_UART1,
		.flags		= UPF_SKIP_TEST,
#endif	// CONFIG_ARCH_CPE
#ifdef CONFIG_ARCH_MOXACPU
		.irq		= IRQ_UART,
		.flags		= UPF_SKIP_TEST | UPF_SHARE_IRQ,
#endif	// CONFIG_ARCH_MOXACPU
		.iotype		= UPIO_PORT,
		.regshift	= 2,
		.uartclk	= CONFIG_UART_CLK,
		.line		= 0,
		.type		= PORT_16550A,
		.fifosize	= 16
#if !(defined CONFIG_ARCH_W311)
	}, {
		.iobase		= CPE_UART2_VA_BASE,
		.mapbase	= CPE_UART2_BASE,
#ifdef CONFIG_ARCH_CPE
		.irq		= IRQ_UART2,
		.flags		= UPF_SKIP_TEST,
#endif	// CONFIG_ARCH_CPE
#ifdef CONFIG_ARCH_MOXACPU
		.irq		= IRQ_UART,
		.flags		= UPF_SKIP_TEST | UPF_SHARE_IRQ,
#endif	// CONFIG_ARCH_MOXACPU
		.iotype		= UPIO_PORT,
		.regshift	= 2,
		.uartclk	= CONFIG_UART_CLK,
		.line		= 1,
		.type		= PORT_16550A,
		.fifosize	= 16
	}
#else	// add by Victor Yu. 06-13-2005
	}
#endif
};
#endif

void __init cpe_map_io(void)
{
	early_serial_setup(&cpe_serial_ports[0]);
#if !(defined CONFIG_ARCH_W311)
	early_serial_setup(&cpe_serial_ports[1]);
#endif
	iotable_init(cpe_io_desc, ARRAY_SIZE(cpe_io_desc));
}

static void __init fixup_cpe(struct machine_desc *desc, struct tag *tags, char **cmdline, struct meminfo *mi)
{
	mi->nr_banks      = 1;
	mi->bank[0].start = 0;
#if defined(CONFIG_ARCH_UC_7112_LX_PLUS) || defined(CONFIG_ARCH_W311) || defined(CONFIG_ARCH_W321) || defined(CONFIG_ARCH_W325) || defined(CONFIG_ARCH_W315)
	mi->bank[0].size  = SZ_32M;
#elif defined(CONFIG_ARCH_IA241_32128) || defined(CONFIG_ARCH_W345_IMP1) || defined(CONFIG_ARCH_IA241_16128)
	mi->bank[0].size  = SZ_128M;
#else	
	mi->bank[0].size  = SZ_64M;
#endif	
	mi->bank[0].node  = 0;
}

#if 1	// add by Victor Yu. 05-25-2005
extern void	cpe_timer_init(void);
#endif
MACHINE_START(MOXACPU, "Moxa CPU development platform")
	MAINTAINER("Victor")
	BOOT_MEM(0x00000000, 0x98200000, 0xf9820000)
	BOOT_PARAMS(0x00000100)
	FIXUP(fixup_cpe)
	MAPIO(cpe_map_io)
	INITIRQ(irq_init_irq)
	INITTIME(cpe_timer_init)
MACHINE_END
