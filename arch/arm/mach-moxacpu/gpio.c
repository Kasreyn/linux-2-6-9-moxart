
#include	<linux/config.h>
#include	<linux/module.h>
#include	<linux/kernel.h>
#include	<linux/types.h>
#include	<linux/spinlock.h>

#include	<asm/io.h>
#include	<asm/arch/cpe/cpe.h>
#include	<asm/arch/gpio.h>

static mcpu_gpio_reg_t	*gpio_reg=(mcpu_gpio_reg_t *)CPE_GPIO_VA_BASE;
static spinlock_t	gpio_lock=SPIN_LOCK_UNLOCKED;

void	mcpu_gpio_inout(u32 gpio, int inout)
{
	spin_lock(&gpio_lock);
	switch ( inout ) {
	case MCPU_GPIO_INPUT :
		writel(readl(&gpio_reg->pin_dir)&~gpio, &gpio_reg->pin_dir);
		break;
	case MCPU_GPIO_OUTPUT :
		writel(readl(&gpio_reg->pin_dir)|gpio, &gpio_reg->pin_dir);
		break;
	}
	spin_unlock(&gpio_lock);
}
EXPORT_SYMBOL(mcpu_gpio_inout);

u32	mcpu_gpio_get_inout(u32 gpio)
{
	u32	ret;

	spin_lock(&gpio_lock);
	if ( readl(&gpio_reg->pin_dir) & gpio )
		ret = MCPU_GPIO_OUTPUT;
	else
		ret = MCPU_GPIO_INPUT;
	spin_unlock(&gpio_lock);
	return ret;
}

void	mcpu_gpio_set(u32 gpio, int highlow)
{
	spin_lock(&gpio_lock);
	switch ( highlow ) {
	case MCPU_GPIO_HIGH :
		writel(readl(&gpio_reg->data_out)|gpio, &gpio_reg->data_out);
		break;
	case MCPU_GPIO_LOW :
		writel(readl(&gpio_reg->data_out)&~gpio, &gpio_reg->data_out);
		break;
	}
	spin_unlock(&gpio_lock);
}
EXPORT_SYMBOL(mcpu_gpio_set);

u32	mcpu_gpio_get(u32 gpio)
{
	u32	ret;

	spin_lock(&gpio_lock);
#if 0	// mask by Victor Yu. 04-20-2007
	ret = readl(&gpio_reg->data_in) & gpio;
#else
	ret = readl(&gpio_reg->pin_dir);
	if ( ret & gpio )	// this is output GPIO
		ret = readl(&gpio_reg->data_out) & gpio;
	else
		ret = readl(&gpio_reg->data_in) & gpio;
#endif
	spin_unlock(&gpio_lock);
	return ret;
}
EXPORT_SYMBOL(mcpu_gpio_get);

void	mcpu_gpio_mp_set(u32 gpio)
{
	spin_lock(&gpio_lock);
	*(volatile unsigned int *)(CPE_PMU_VA_BASE+0x100) |= gpio;
	spin_unlock(&gpio_lock);
}
EXPORT_SYMBOL(mcpu_gpio_mp_set);

void	mcpu_gpio_mp_clear(u32 gpio)
{
	spin_lock(&gpio_lock);
	*(volatile unsigned int *)(CPE_PMU_VA_BASE+0x100) &= ~gpio;
	spin_unlock(&gpio_lock);
}
EXPORT_SYMBOL(mcpu_gpio_mp_clear);
