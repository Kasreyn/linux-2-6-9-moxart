/*
 * This is Moxa IA261 DI/DO device driver.
 * It is from misc interface. So the device node major number is 8.
 * The device node minor number is following:
 * dio:		104
 *
 * History:
 * Date		Aurhor			Comment
 * 12-13-2007	Victor Yu.		Create it.
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/rtc.h>
#include <linux/timer.h>
#include <linux/ioport.h>
#include <linux/proc_fs.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/arch/moxa-ia261.h>

#define MOXA_DIO_MINOR		104

static unsigned int		di_addr=MOXA_IA261_UART_DIO_VIRT_BASE + 0x60; 
static unsigned int		do_addr=MOXA_IA261_UART_DIO_VIRT_BASE + 0x50; 
static unsigned char		do_state_keep=0;

//
// DIO file operaiton function call
//
#define MAX_DIO			8

#define DIO_INPUT		1
#define DIO_OUTPUT		0
#define DIO_HIGH		1
#define DIO_LOW			0
#define IOCTL_DIO_GET_MODE      1
#define IOCTL_DIO_SET_MODE      2
#define IOCTL_DIO_GET_DATA      3
#define IOCTL_DIO_SET_DATA      4
#define IOCTL_SET_DOUT		15
#define IOCTL_GET_DOUT		16
#define IOCTL_GET_DIN		17
struct dio_set_struct {
	int	io_number;
	int	mode_data;	// 1 for input, 0 for output, 1 for high, 0 for low
};


struct proc_dir_entry* dio_proc;
#define dio_proc_name "driver/dio"
int dio_read_proc(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	char		*out = page;
	int		pin_count, len;
	unsigned char	di_state;
 
	for ( pin_count = 0; pin_count < MAX_DIO; pin_count++ )
		out += sprintf(out, "DO[%d]:%s\n", pin_count, do_state_keep&(1<<pin_count)?"HIGH":"LOW");  
	di_state = inb(di_addr);
	for ( pin_count = 0; pin_count < MAX_DIO; pin_count++ )
		out += sprintf(out, "DI[%d]:%s\n", pin_count, di_state&(1<<pin_count)?"HIGH":"LOW");  

	len = out -page - off;
	if (len < count)  {
		*eof = 1;
		if (len <= 0) 
			return 0;
	}  else
		len = count;
	*start = page + off;

	return len;
}

int dio_write_proc(struct file *file, const char __user *buffer, unsigned long count, void *data)
{
	char	kbuffer[64], mode[2], pin[3];
	int	num_count, var_num, byte_count;

	if ( copy_from_user(kbuffer, (char*)buffer , count) )
		return -EFAULT;

	kbuffer[count+1] = '\0';
	memset(pin,0,3);
	memset(mode,0,2);
	num_count = var_num = 0 ;

	for ( byte_count = 0 ; byte_count < count ; byte_count++ ) {
		if ( kbuffer[byte_count] != ' ' && kbuffer[byte_count] != '\0' && kbuffer[byte_count] != '\n' ) {
			if ( var_num == 0 ) {
				num_count++;     
				if ( num_count > 2 ) {
					printk("The max pin number is %d !!\n",MAX_DIO);	
					return -EINVAL;
				}			      
				pin[num_count-1] =  kbuffer[byte_count];   
				pin[num_count] = '\0';
			}
			if ( var_num == 1 ) {	// to read high or low for output(DO) mode
				num_count++;     
				if ( num_count > 1 ) {
					printk("The high = %d, low = %d !!\n", DIO_HIGH, DIO_LOW);	
					return -EINVAL;
				}			      
				mode[num_count-1] =  kbuffer[byte_count];   
				mode[num_count] =  '\0';   
			}
		} else {
			if ( pin[0] != 0 ) {
				var_num = 1;
				num_count = 0;
			}
		}
	}

	num_count = simple_strtol(pin, NULL, 0);	// to keep pin number
	byte_count = simple_strtol(mode, NULL, 0);	// to keep state setting

	if ( num_count >= MAX_DIO ) {
		//printk("The max pin number is %d !!\n",MAX_GPIO-1);	
		goto error_write_proc;
	}
		
	if ( byte_count == DIO_HIGH )
		do_state_keep |= (1 << num_count);
	else if ( byte_count == DIO_LOW )
		do_state_keep &= !(1<< num_count);
	else
		goto error_write_proc;
	outb(do_state_keep, do_addr);

	return count;

error_write_proc:
	return -EINVAL;
}

static int dio_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	struct dio_set_struct	set;
	unsigned char		di_state;

	switch ( cmd ) {
	case IOCTL_SET_DOUT :
		if ( copy_from_user(&set, (struct dio_set_struct *)arg, sizeof(struct dio_set_struct)) )
			return -EFAULT;
		if ( set.io_number < 0 || set.io_number >= MAX_DIO )
			return -EINVAL;
		if ( set.mode_data == DIO_HIGH )
			do_state_keep |= (1<<set.io_number);
		else if ( set.mode_data == DIO_LOW )
			do_state_keep &= !(1<<set.io_number);
		else
			return -EINVAL;
		outb(do_state_keep, do_addr);
		break;
	case IOCTL_GET_DOUT :
	case IOCTL_GET_DIN :
		if ( copy_from_user(&set, (struct dio_set_struct *)arg, sizeof(struct dio_set_struct)) )
			return -EFAULT;
		if ( set.io_number < 0 || set.io_number >= MAX_DIO )
			return -EINVAL;
		if ( cmd == IOCTL_GET_DOUT ) {
			if ( do_state_keep & (1<<set.io_number) )
				set.mode_data = 1;
			else
				set.mode_data = 0;
		} else {
			di_state = inb(di_addr);
			if ( di_state & (1<<set.io_number) )
				set.mode_data = 1;
			else
				set.mode_data = 0;
		}
		if ( copy_to_user((struct dio_set_struct *)arg, &set, sizeof(struct dio_set_struct)) )
			return -EFAULT;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int dio_open(struct inode *inode, struct file *file)
{
	if ( MINOR(inode->i_rdev) == MOXA_DIO_MINOR )
		return 0;

	return -ENODEV;
}

static int dio_release(struct inode *inode, struct file *file)
{
	return 0;
}

static struct file_operations dio_fops = {
	owner:THIS_MODULE,
	llseek:NULL,
	ioctl:dio_ioctl,
	open:dio_open,
	release:dio_release,
};

static struct miscdevice dio_dev = {
	MOXA_DIO_MINOR,
	"dio",
	&dio_fops
};

static void __exit dio_exit(void)
{
	remove_proc_entry(dio_proc_name,0);
	misc_deregister(&dio_dev);
}

static int __init dio_init(void)
{
	printk("Moxa IA261 Register DI/DO misc ver1.0 ");
	if ( misc_register(&dio_dev) ) {
		printk("fail !\n");
		return -ENOMEM;
	}
	
	outb(do_state_keep, do_addr);
	dio_proc = create_proc_entry(dio_proc_name, 0, NULL);
	dio_proc->read_proc = dio_read_proc;
	dio_proc->write_proc = dio_write_proc;

	printk("OK.\n");
	return 0;
}

module_init(dio_init);
module_exit(dio_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
