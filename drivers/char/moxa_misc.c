/*
 * This is Moxa CPU device driver.
 * This device driver will control the READY LED & RESET button.
 * Also this device driver will control debug LED.
 * It is from misc interface. So the device node major number is 10.
 * The device node minor number is following:
 * mxmisc:	105
 *
 * History:
 * Date		Aurhor			Comment
 * 02-22-2006	Victor Yu.		Create it.
 */
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/rtc.h>
#include <linux/timer.h>
#include <linux/ioport.h>
#include <linux/kmod.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/arch/cpe/cpe.h>
#include <asm/arch/gpio.h>

#define MOXA_MISC_MINOR		105

#if (defined CONFIG_ARCH_IA241_32128)||(defined CONFIG_ARCH_IA241_16128)// add by Victor Yu. 05-22-2007
#define CONFIG_ARCH_IA241
#endif

#if (defined CONFIG_ARCH_UC_7112_LX_PLUS_LITON)
#define CONFIG_ARCH_UC_7112_LX_PLUS
#endif

#if defined(CONFIG_ARCH_IA240) || defined(CONFIG_ARCH_IA241) || defined(CONFIG_ARCH_W341) || defined(CONFIG_ARCH_W345) || defined(CONFIG_ARCH_W345_IMP1) || defined(CONFIG_ARCH_UC_7112_LX_PLUS) || defined(CONFIG_ARCH_W321)|| defined(CONFIG_ARCH_W311) || defined(CONFIG_ARCH_W325) || defined(CONFIG_ARCH_W315)
#define SW_READY_GPIO		(1<<27)
#define SW_RESET_GPIO		(1<<25)
#else
#define SW_READY_GPIO		(1<<4)
#define SW_RESET_GPIO		(1<<23)
#endif

#if defined(CONFIG_ARCH_IA240) || defined(CONFIG_ARCH_IA241)	// mask by Victor Yu. 04-20-2007
#define DEBUG_LED_0		(1<<0)
#define DEBUG_LED_1		(1<<1)
#define DEBUG_LED_2		(1<<2)
#define DEBUG_LED_3		(1<<3)
#define DEBUG_LED		(DEBUG_LED_0|DEBUG_LED_1|DEBUG_LED_2|DEBUG_LED_3)
#endif

#define RESET_POLL_TIME		(HZ/5)
#define RESET_TIMEOUT		(HZ * 5)

//
// file operaiton function call
//
#define IOCTL_SW_READY_ON	1
#define IOCTL_SW_READY_OFF	2

#if defined(CONFIG_ARCH_IA240) || defined(CONFIG_ARCH_IA241)//0	// mask by Victor Yu. 04-20---2007
#define IOCTL_DEBUG_LED_OUT	0x100
#endif

static struct timer_list	resettimer;
static spinlock_t		resetlock=SPIN_LOCK_UNLOCKED;
static unsigned long		endresettime, intervaltime;
static int			ledonoffflag;
static struct work_struct	resetqueue;

static void     settodefault(void *unused)
{
        char    *argv[2], *envp[5];

        if ( in_interrupt() )
                return;
        if ( !current->fs->root )
                return;
        argv[0] = "/bin/setdef";
        argv[1] = 0;
        envp[0] = "HOME=/";
        envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
        envp[2] = 0;
        call_usermodehelper(argv[0], argv, envp, 0);
}

static void reset_poll(unsigned long ingore)
{
	spin_lock(&resetlock);
	del_timer(&resettimer);
	if ( !mcpu_gpio_get(SW_RESET_GPIO) ) {
		if ( endresettime == 0 ) {
			endresettime = jiffies + RESET_TIMEOUT;
			intervaltime = jiffies + HZ;
		} else if ( time_after(jiffies, endresettime) ){
			mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_HIGH);
			schedule_work(&resetqueue);
			goto poll_exit;
		} else if ( time_after(jiffies, intervaltime) ) {
			if ( ledonoffflag ) {
				ledonoffflag = 0;
				mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_HIGH);
			} else {
				ledonoffflag = 1;
				mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_LOW);
			}
			intervaltime = jiffies + HZ;
		}
	} else if ( endresettime ) {
		endresettime = 0;
		ledonoffflag = 1;
		mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_LOW);
	}
	resettimer.function = reset_poll;
	resettimer.expires = jiffies + RESET_POLL_TIME;
	add_timer(&resettimer);
poll_exit:
	spin_unlock(&resetlock);
}

static int mxmisc_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	switch ( cmd ) {
	case IOCTL_SW_READY_ON :
		mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_LOW);
		break;
	case IOCTL_SW_READY_OFF :
		mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_HIGH);
		break;
#if defined(CONFIG_ARCH_IA240) || defined(CONFIG_ARCH_IA241)//0	// mask by Victor Yu. 04-20-2007
	case IOCTL_DEBUG_LED_OUT :
		{
		unsigned int	val;
		if ( copy_from_user(&val, (unsigned int *)arg, sizeof(unsigned int)) )
			return -EFAULT;
		if ( val & 1 )
			mcpu_gpio_set(DEBUG_LED_0, MCPU_GPIO_LOW);
		else
			mcpu_gpio_set(DEBUG_LED_0, MCPU_GPIO_HIGH);
		if ( val & 2 )
			mcpu_gpio_set(DEBUG_LED_1, MCPU_GPIO_LOW);
		else
			mcpu_gpio_set(DEBUG_LED_1, MCPU_GPIO_HIGH);
		if ( val & 4 )
			mcpu_gpio_set(DEBUG_LED_2, MCPU_GPIO_LOW);
		else
			mcpu_gpio_set(DEBUG_LED_2, MCPU_GPIO_HIGH);
		if ( val & 8 )
			mcpu_gpio_set(DEBUG_LED_3, MCPU_GPIO_LOW);
		else
			mcpu_gpio_set(DEBUG_LED_3, MCPU_GPIO_HIGH);
		break;
		}
#endif
	default:
		return -EINVAL;
	}
	return 0;
}

static int mxmisc_open(struct inode *inode, struct file *file)
{
	if ( MINOR(inode->i_rdev) == MOXA_MISC_MINOR )
		return 0;
	return -ENODEV;
}

static int mxmisc_release(struct inode *inode, struct file *file)
{
	return 0;
}

static struct file_operations mxmisc_fops = {
	owner:THIS_MODULE,
	llseek:NULL,
	ioctl:mxmisc_ioctl,
	open:mxmisc_open,
	release:mxmisc_release,
};
static struct miscdevice mxmisc_dev = {
	MOXA_MISC_MINOR,
	"mxmisc",
	&mxmisc_fops
};

static void __exit mxmisc_exit(void)
{
	spin_lock(&resetlock);
	del_timer(&resettimer);
	spin_unlock(&resetlock);
	misc_deregister(&mxmisc_dev);
}

static int __init mxmisc_init(void)
{
	printk("Register Moxa misc ver1.0 ");
	if ( misc_register(&mxmisc_dev) ) {
		printk("fail !\n");
		return -ENOMEM;
	}
	
	// set the CPU for GPIO
	mcpu_gpio_mp_set(SW_READY_GPIO|SW_RESET_GPIO);
	// default set all GPIO for input/ouput
	mcpu_gpio_inout(SW_READY_GPIO, MCPU_GPIO_OUTPUT);
	mcpu_gpio_inout(SW_RESET_GPIO, MCPU_GPIO_INPUT);
	mcpu_gpio_set(SW_READY_GPIO, MCPU_GPIO_HIGH);

#if defined(CONFIG_ARCH_IA240) || defined(CONFIG_ARCH_IA241)//0	// mask by Victor Yu. 04-20-2007
	// set the debug led output
	mcpu_gpio_inout(DEBUG_LED, MCPU_GPIO_OUTPUT);
#endif

	// initialize the reset polling
	INIT_WORK(&resetqueue, settodefault, NULL);
	spin_lock(&resetlock);
	endresettime = 0;
	ledonoffflag = 1;
	init_timer(&resettimer);
	resettimer.function = reset_poll;
	resettimer.expires = jiffies + RESET_POLL_TIME;
	add_timer(&resettimer);
	spin_unlock(&resetlock);

	printk("OK.\n");
	return 0;
}

module_init(mxmisc_init);
module_exit(mxmisc_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
