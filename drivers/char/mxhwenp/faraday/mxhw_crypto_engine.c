#include <linux/config.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/delay.h> 
#include <asm/system.h>
#include <asm/io.h>				// readl, writel
#include <asm/irq.h>

#include <asm/arch/irq.h>
#include <asm/arch/cpe/cpe.h>   //All register definition
#include <asm/arch/cpe_int.h>   //Interrupt definition
#include <asm/arch/cpe/a320c.h>

#include "mxhw_crypto_engine.h"

/* memory and register information */
#define CPE_WMAC_BASE   0x90F00000			/* physical base address */
#define REGS_SIZE  		0x90				/* physical area */
static 	void			*base=NULL;			/* base address of the io memory assigned to the engine */
#define REGWRL(off,v)	writel(v,base+off)
#define REGRDL(off)		readl(base+off)

#define PERFORM_WAIT_TIME	50
#define MAX_WAIT_TIMES		200

#ifdef IRQUSED 
#define IRQ_WMAC		29					/* trq number */
wait_queue_head_t 		dmawq;				/* a waiting queue that the engine wakes up the dispatcher */
static 	u32 			IRQ_INT_FLAG=0;
#endif

#ifdef DEBUG
static void
read_registers(u32 a, u32 b)
{
	if (a==0 && b==0) b = REGS_SIZE;
		
	mdelay(200);
	printk("\n-------------- addresses --------------%d\n", 0);
	for (; a < b; a+=4)
		printk("0x%02x = 0x%08x\n", a, REGRDL(a));
	printk("\n-------------- addresses --------------%d\n", 9);
	mdelay(200);
}
#else
#define read_registers(a,b)
#endif

int
mxhw_crypto_engine_dma_malloc(IOMBUF *mbuf, u32 size)
{
	IOMBUF_OFFS(mbuf) = 0;
	IOMBUF_SIZE(mbuf) = size; /* memory space for data */
	IOMBUF_VIRT(mbuf) = consistent_alloc( GFP_DMA|GFP_KERNEL, size, 
		&(IOMBUF_PHYS(mbuf)));
	if (IOMBUF_VIRT(mbuf)==NULL)
		return 1;
	return 0;
}

void 
mxhw_crypto_engine_dma_mfree(IOMBUF *mbuf)
{
	if (mbuf && IOMBUF_VIRT(mbuf))	
		consistent_free((u32 *)IOMBUF_VIRT(mbuf), IOMBUF_SIZE(mbuf)+IOMBUF_OFFS(mbuf), IOMBUF_PHYS(mbuf));
}

typedef struct _CTXPARAM
{
	u32	algo;
	u32	ctrl;
	u32	blen;
	u32	klen;
} CTXPARAM;

static CTXPARAM CtxParams[] =
{
	{MXCIPHER_ALGO_DES,		_BIT_RW_MXCIPHER_ALGO_DES,		8,	8},
	{MXCIPHER_ALGO_3DES,	_BIT_RW_MXCIPHER_ALGO_3DES,		8,	24},
	{MXCIPHER_ALGO_AES128,	_BIT_RW_MXCIPHER_ALGO_AES128,	16, 16},
	{MXCIPHER_ALGO_AES192,	_BIT_RW_MXCIPHER_ALGO_AES192,	16, 24},
	{MXCIPHER_ALGO_AES256,	_BIT_RW_MXCIPHER_ALGO_AES256,	16, 32}
};

static u32 CtxParams_Size=(sizeof(CtxParams)/sizeof(CtxParams[0]));

#if 1
#define SWAP32(v) ((v>>24)|((v&0x00ff0000)>>8)|((v&0x0000ff00)<<8)|(v<<24))
#else
#define SWAP32(v) v
#endif

static void
swap(u8 *data, u32 len)
{
	u32 i, v;
	
	for (i=0; i < len; i+=4, data += 4)
	{
		v = *(u32*) data;
		*(u32*)data = SWAP32(v);
	}
}

#define mswap32(data,l) \
{ \
	u32 i, v, len=l>>2; \
	u32 *d = (u32*) data; \
	for (i=0; i < len; i++, d++) \
	{ \
		v = *d; \
		*d = SWAP32(v); \
	} \
} \

int
mxhw_crypto_engine_register(CIPHER *info, u32 *ctrl)
{
	u32	i;

	if (info->mode==MXCIPHER_MODE_ECB) 		*ctrl = _BIT_RW_MXCIPHER_MODE_ECB;
	else if (info->mode==MXCIPHER_MODE_CBC) *ctrl = _BIT_RW_MXCIPHER_MODE_CBC;
	else if (info->mode==MXCIPHER_MODE_CTR) *ctrl = _BIT_RW_MXCIPHER_MODE_CTR;
	else if (info->mode==MXCIPHER_MODE_OFB) *ctrl = _BIT_RW_MXCIPHER_MODE_OFB;
	else if (info->mode==MXCIPHER_MODE_CFB) 
	{
		if (info->bits!=8)
			return -1;
		*ctrl = _BIT_RW_MXCIPHER_MODE_CFB;
	}
	else
		return -1;

	*ctrl |= (info->type==0)? _BIT_RW_MXCIPHER_TYPE_DEC:_BIT_RW_MXCIPHER_TYPE_ENC;

	info->blen = info->klen = 0;
	for (i=0; i < CtxParams_Size;i++)
	{
		if (info->algo==CtxParams[i].algo)
		{
			info->blen = CtxParams[i].blen;
			info->klen = CtxParams[i].klen;
			*ctrl |= CtxParams[i].ctrl;
			swap(info->keys, info->klen);
			return 0;
		}
	}
	return -2;
}

void
mxhw_crypto_engine_unregister(u32 ctrl)
{
	(void) ctrl;
}

#define IRQ_LEVEL 			LEVEL
#define IRQ_ACTIVE 			L_ACTIVE
#define INTRPT_ENBL_BIT 	1
#define INTRPT_DSBL_BIT 	0 

/* Note: 
		1. IOMBUF_DLEN(imbuf) covers the IV if there is 
		2. remember to set IOMBUF_DLEN(ombuf) = IOMBUF_DLEN(imbuf)
*/
int
mxhw_crypto_engine_perform(u32 ctrl, CIPHER *info, IOMBUF *imbuf, IOMBUF *ombuf, u32 *exited)
{
	u_long	offs;
	u32		tmpv,dlen;
	u8		*ivec;
	int 	i,num=0;

	if (info->mode!=MXCIPHER_MODE_ECB) 
		dlen = IOMBUF_DLEN(imbuf)-info->blen;
	else
	{
		dlen = IOMBUF_DLEN(imbuf);
		if (info->mode!=MXCIPHER_MODE_CFB && dlen%info->blen)
			return 1;
	}
	 
	DBG(KERN_INFO "mxhw_engine: engine_perform = %d %d 0x%x\n", 11, info->type, ctrl);

	/* step 0: disable all interrupts: stop, err, done */
	REGWRL(_REG_WO_INTRPT_CLEAR, 1); 

	/* step 1: set cipher control */
	REGWRL(_REG_RW_MXCIPHER_CTRL, ctrl);

	/* step 2: set IVs if there are, 4 bytes each */
	if (info->mode!=MXCIPHER_MODE_ECB)
	{
		dlen = IOMBUF_DLEN(imbuf)-info->blen;
		ivec = IOMBUF_DATA(imbuf)+dlen;
		for (i=0,offs=_REG_RW_MXCIPHER_IVIN0; i < info->blen; i+=4, offs+=4)
		{
			tmpv = *(u32*)(ivec+i);
			tmpv = SWAP32(tmpv);
			REGWRL(offs,tmpv);
		}
	}
	
	/* step 3:	set keys, 4 bytes each, swap already  */
	for (i=0,offs=_REG_RW_MXCIPHER_KEY0; i < info->klen; i+=4, offs+=4) 
		REGWRL(offs,*(u32*)(info->keys+i));

	/* step 4:	set DMA controls */ 
	REGWRL( _REG_RW_DMA_ADDR_SURC,	IOMBUF_PHYS(imbuf));
	REGWRL( _REG_RW_DMA_ADDR_DEST,	IOMBUF_PHYS(ombuf));
	REGWRL( _REG_RW_DMA_DATA_SIZE,	dlen);
	
	/* step 5:	enable transfer done */
	REGWRL(_REG_RW_INTRPT_ENBL, INTRPT_ENBL_BIT);   

	DBG(KERN_INFO "mxhw_engine: engine_perform = %d\n", 66);
	
	/* step 6:	enable DMA engine  */
	REGWRL(_REG_RW_DMA_ENGN_CTRL, _BIT_RW_DMA_ENGN_ENBL);
//read_registers(_REG_RW_DMA_ENGN_CTRL,0x70);
	
	/* step 7:	wait for data of the size is transferred. if it waits for 
		a stop at DMA, the data might not be completely transferred */
#ifdef IRQUSED 
	DBG(KERN_INFO "mxhw_engine: interrupt = %d %d\n", 77, dlen);
#if 0
	wait_event_interruptible(&dmawq, (REGRDL(_REG_RO_INTRPT_POST)&_BIT_RO_INTRPT_DONE)!=0);
#else
	while(IRQ_INT_FLAG==0 && num++ < MAX_WAIT_TIMES)
	{
		udelay(PERFORM_WAIT_TIME); // current->state = TASK_INTERRUPTIBLE; schedule_timeout(HZ); 
		if (IRQ_INT_FLAG!=0)
			break;
	}
#endif
	IRQ_INT_FLAG=0;

#else
	DBG(KERN_INFO "mxhw_engine: poll =%d %d\n", 77, dlen);

	while(num++ < MAX_WAIT_TIMES)
	{
		udelay(PERFORM_WAIT_TIME);
		if ((REGRDL(_REG_RO_INTRPT_POST)&_BIT_RO_INTRPT_DONE)!=0) 
			break;
	}
#endif 
	/* set output length */
	IOMBUF_DLEN(ombuf) = IOMBUF_DLEN(imbuf);
	
	if (num >= MAX_WAIT_TIMES)
	{
		printk("mxhw_engine: timeout!! data length= %d\n",dlen);
		return 2;
	}

	/* step 8: get IVs if there are, 4 bytes each */
	if (info->mode != MXCIPHER_MODE_ECB) 
	{
		ivec = IOMBUF_DATA(ombuf)+dlen;
		for (i=0,offs=_REG_RO_MXCIPHER_IVOUT0; i < info->blen; i+=4, offs+=4)
		{
			tmpv = REGRDL(offs);
			*(u32*)(ivec+i) = SWAP32(tmpv);
		}
	}
	return 0;
}

#ifdef IRQUSED
/* hw interrupt notifies a complete of packet,
	this interrupt handler represents no process, so can't sleep,
	no kmalloc, current->pid, copy_from_user, ..., and etc
*/
static void
mxhw_crypto_engine_interrupt(int irq, void *dev_id, struct pt_regs *regs_base)
{
//	printk(KERN_INFO "mxhw_crypto_engine_interrupt irq = %d\n", irq);
	
	/* check if it is what the engine says */
	if (IRQ_INT_FLAG==0 && (REGRDL(_REG_RO_INTRPT_POST)&_BIT_RO_INTRPT_DONE)!=0)
	{
		wake_up_interruptible(&dmawq);
		REGWRL(_REG_WO_INTRPT_CLEAR, 1); 
		IRQ_INT_FLAG=1; 
	}
}
#endif 

int 
mxhw_crypto_engine_up(void)
{
	/* get the base mapping address to the engine */
	if ((base=(void*)ioremap_nocache(CPE_WMAC_BASE, REGS_SIZE))==NULL)
	{
		printk("Fail to io map\n");
		return -EFAULT;
	} 
	memset_io(base, 0, REGS_SIZE);
	
	/* set initial FIFO configuration for the crypto engine */
  	REGWRL(_REG_RW_IOFIFO_THRD, 0x101);
  	
#ifdef IRQUSED 
	/* set up interrupt mechanism */
	cpe_int_set_irq(IRQ_WMAC, LEVEL, H_ACTIVE);
	if (request_irq(IRQ_WMAC, (void*) mxhw_crypto_engine_interrupt, SA_INTERRUPT,
					"mxcrypto", NULL)!=0)
	{ /* it return -EBUSY */
		printk("Some other device may take this line of IRQ\n");
		return 2; 
	}
	/* bring up the waiting queue for engine and the dispatcher */
	init_waitqueue_head(&dmawq);
	printk("setup IRQ\n"); 
#endif
	return 0;
} 

void
mxhw_crypto_engine_down(void)
{
	REGWRL(_REG_RW_DMA_ENGN_CTRL,_BIT_RW_DMA_ENGN_DSBL);
#ifdef IRQUSED
	/* the thread might sleep on waiting for DMA */
	wake_up_interruptible(&dmawq);
	free_irq(IRQ_WMAC, NULL);
	unregister_chrdev(CRYPTO_MAJOR, CRYPTO_DEVNAME);
#endif
	/* free up memory */
	if (base) iounmap(base);
	/* disable DMA and security engines */
}

