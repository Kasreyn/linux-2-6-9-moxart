#ifndef _H_TEST_SSL_
#define _H_TEST_SSL_
#include <stdlib.h>
#include <stdio.h>
#include <openssl/bio.h>
#include <des/des.h>
#include <des/des_locl.h>
#include <openssl/opensslv.h>

#if OPENSSL_VERSION_NUMBER >= 0x00907000L
#include <aes/aes.h>
#else
#define DES_cblock 				des_cblock
#define const_DES_cblock 		const_des_cblock
#define DES_set_key_unchecked 	des_set_key_unchecked
#define DES_key_schedule 		des_key_schedule
#endif

#define TCIPHER(f) void f (const u_char *input,u_char *output,long length,\
							void *ks1,void *ks2,void *ks3,u_char *ivec,int enc)

TCIPHER(test_DES_ecb_encrypt);
TCIPHER(test_DES_cbc_encrypt);
TCIPHER(test_DES_ecb3_encrypt);
TCIPHER(test_DES_ede3_cbc_encrypt);

#ifdef CPE_ENGINE
TCIPHER(test_DES_cfb_encrypt);
TCIPHER(test_DES_ofb_encrypt);
TCIPHER(test_DES_cfb64_encrypt);
TCIPHER(test_DES_ede3_ofb64_encrypt);
TCIPHER(test_DES_ede3_cfb64_encrypt);
#endif

#if OPENSSL_VERSION_NUMBER >= 0x00907000L
TCIPHER(test_AES_ecb_encrypt);
TCIPHER(test_AES_cbc_encrypt);
TCIPHER(test_AES_ctr_encrypt);
#ifdef CPE_ENGINE
TCIPHER(test_AES_cfb_encrypt);
TCIPHER(test_AES_ofb_encrypt);
TCIPHER(test_AES_cfb128_encrypt);
#endif
#endif

#define MAX_BUF 1536
#define TYPE_ENCRYPT 1
#define TYPE_DECRYPT 0
#define ISSW 1
#define ISHW 0

#define TSTART 	0
#define TEND 	1

typedef struct _TESTHW
{
	u_int 	algo;
	u_int 	mode;
	char	name[32];
	u_int 	blen;
	u_int 	klen;
	u_int	aes_dec;
	TCIPHER	((*do_cipher));
	void	*key1, *key2, *key3;
    DES_key_schedule 	DESks1,DESks2,DESks3;
#if OPENSSL_VERSION_NUMBER >= 0x00907000L
    AES_KEY				AESKey1,AESKey2;
#endif
} TESTHW;

//void	hexit(char *msg, u_char *d, int len);
void	swap(u_char *data, u_int len);
u_int	fill_data(u_char *buf, u_int size);
void	times_reset();
void	timeit(u_int hwsw, u_int idx, int type, int add);
void	test_set_keys(TESTHW *t, u_char *keys, int IS_AES);
void	timereport(char *name, u_int *pktSizes, u_int max, u_int numPkt,u_int);
u_int	timestamps();
void*	test_set_decrypt_key(TESTHW *t, u_int *dectype, int IS_AES);
#endif
