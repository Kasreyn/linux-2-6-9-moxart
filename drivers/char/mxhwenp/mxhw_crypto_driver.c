#include <linux/config.h>
#include <linux/version.h>
#ifdef MODULE
#include <linux/module.h>
#endif
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fcntl.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <asm/system.h>
#include <asm-generic/smplock.h>
#include <asm/uaccess.h>		// verify_area, copy_from_user, ... 

#include <linux/miscdevice.h>

#include <mxhw_crypto_driver.h>

global_p global;

static DECLARE_COMPLETION(dispatcher_exited);

#ifdef DEBUG
void
hexit(char *msg, u_char *d, int len)
{
	int i;

	mdelay(10);
	DBG("[%d] %s ",len, msg);
	len = len>80? 80:len;
	for (i=0; i< len; i+=4)
		DBG("%08x ",*(u_int*)(d+i));
	DBG("\n");
	mdelay(10);
}
#endif

/* free up a context buffer */
static void
mxhw_crypto_qmctx_free(QMBCTX *c)
{
	if (!c)
		return;

	if (&c->imbuf) mxhw_crypto_engine_dma_mfree(&c->imbuf);
	if (&c->ombuf) mxhw_crypto_engine_dma_mfree(&c->ombuf);

	kfree(c);
}

/* allocate a context buffer */
static QMBCTX*
mxhw_crypto_qmctx_init(u32 d_size)
{
	QMBCTX	*c = (QMBCTX*) kmalloc(sizeof(QMBCTX), GFP_KERNEL);

	if (!c)
		return NULL;
	memset(c, 0, sizeof(QMBCTX));
	if (mxhw_crypto_engine_dma_malloc(&c->imbuf, d_size)!=0 ||
		mxhw_crypto_engine_dma_malloc(&c->ombuf, d_size)!=0)
	{
		mxhw_crypto_qmctx_free(c);
		c = NULL;
	}
	return c;
}

/* free up a queue of context buffers which are linked list */
static void
mxhw_crypto_qmptr_free(QMBPTR *q)
{
	QMBCTX	*c;

	if (!q)
		return;
	for(c=q->head;c!=NULL;c = c->next)
		mxhw_crypto_qmctx_free(c);
	kfree(q);
}

/* new a queue with a lock and a wait queue */
static QMBPTR*
mxhw_crypto_qmptr_init(u32 numq, u32 d_size)
{
	QMBPTR	*q;
	QMBCTX	*c;
	u32		i;

	if ((q=(QMBPTR*) kmalloc(sizeof(QMBPTR), GFP_KERNEL))==NULL)
		return NULL;

	memset(q, 0, sizeof(QMBPTR));
	/* apply lock when enqueue/dequeue */
	spin_lock_init(&q->lock);
	/* wake up waiting processes */
	init_waitqueue_head(&q->quew);

	/* allocate a linked list of context buffers */
	for (i=0; i<numq; i++)
	{
		c = mxhw_crypto_qmctx_init(d_size);
		if (c==NULL)
		{
			mxhw_crypto_qmptr_free(q);
			return NULL;
		}
		ENQUEUE_CONTEXT(q,c);
	}
	return q;
}

IOCTRL*
mxhw_crypto_iocall_op(u32 cpid, int type)
{
	static	spinlock_t iolock;
	static	u32 first=1;
	IOCTRL	*p = NULL;
	u32		i;

	if (first)
	{
		spin_lock_init(&iolock);
		first=0;
	}

	spin_lock(&iolock);
	
	for (i=0;i<MAX_CIPHER_CLIENTS;i++)
	if (global.pool[i].cpid==cpid)
	{
		p = &global.pool[i];
		break;
	}
	switch(type)
	{
	case IOCTRL_OP_ALIVE:
		break;
	case IOCTRL_OP_CLOSE:
		if (p)
		{
			/* if there are un-read contexts, return them to the free pool */
			if (p->outq->head)
			{
				ENQUEUE_CONTEXT(global.free_ctxq, p->outq->head);
				p->outq->head=p->outq->tail=0; /* make sure */
			}
			p->cpid = 0; /* make this buffer available */
		}
		break;
	case IOCTRL_OP_CHKIN:
		if (p)
		{
			if (p->outq==NULL && (p->outq=mxhw_crypto_qmptr_init(0,0))==NULL)
				p = NULL;
			else
			{
				p->cpid = (i<<16|current->pid);
				p->pkt_num=0;
			}
		}
		break;
	case IOCTRL_OP_FREEQ:
		for (i=0;i<MAX_CIPHER_CLIENTS;i++)
		{
			p = &global.pool[i];
			if (p->outq) mxhw_crypto_qmptr_free(p->outq);
		}
		break;
	}
	spin_unlock(&iolock);
	return p;
}

/*	a process that issues a cipher request would compete with any other 
	for resource (free context buffers)
 */
static __inline__ ssize_t 
mxhw_crypto_write(struct file * filp, const char * buf, size_t count, loff_t *pos)
{
	IOCTRL	*ictx = (IOCTRL*)filp->private_data;
	IOMBUF	*mbuf;
	QMBCTX	*qctx;
	QMBPTR	*qptr;
	u32		tout;
	ssize_t r=0;

	(void) pos;
	
	if (verify_area(VERIFY_READ, buf, count))
		return -EFAULT;

	tout = (filp->f_flags & O_NONBLOCK)? 10:0;
	
	/*	get a context buffer from the free pool */
	DEQUEUE_CONTEXT(global.free_ctxq, qctx, tout, 0); 
	if (qctx==NULL) /* non-blocking mode? */
		return ((tout>0)? -EAGAIN:-EFAULT); 
		
	mbuf = &qctx->imbuf;
	/* an oversize packet, realloc memory */
	if (count > IOMBUF_SIZE(mbuf))
	{
		QMBCTX *c = mxhw_crypto_qmctx_init(count);
		if (c!=NULL)
		{	/* create a new one, free old one */
			mxhw_crypto_qmctx_free(qctx);
			qctx = c;
		}
		else
			r = -ENOMEM;
	}
	if (r==0)
	{
		/* dlen+ilen */
		IOMBUF_DLEN(mbuf) = count;		/* mark data length every time */
		if (copy_from_user(IOMBUF_DATA(mbuf),buf,count)==0)
		{
			/* make a copy of the cipher control, also mark the ownership */
			memcpy(&qctx->ictx, ictx, sizeof(IOCTRL));
			r = count;
		}
		else
			r = -EFAULT;
	}
	qptr = (r<0)? global.free_ctxq:global.dspt_ctxq;
	/* enqueue this packet into the dispatcher list or into the free pool */
	ENQUEUE_CONTEXT(qptr, qctx);

	return r;
}

/* sequential access to the processed packets */
static __inline__ ssize_t 
mxhw_crypto_read(struct file *filp, char *buf, size_t count, loff_t *pos)
{
	IOCTRL	*ictx = (IOCTRL*) filp->private_data;
	QMBCTX	*qctx;
	u32		tout;
	ssize_t	r = -EFAULT;

	(void) pos;
	
	if (verify_area(VERIFY_WRITE, buf, count))
		return -EFAULT;

	tout = filp->f_flags&O_NONBLOCK? 10:0;
	/* get the next packet from the caller's output list */
	DEQUEUE_CONTEXT(ictx->outq, qctx, tout, 0);
	if (qctx==NULL)
		return ((tout>0)? -EAGAIN:-EFAULT); 
	else if (qctx->status == 0)
	{
		IOMBUF	*mbuf=&qctx->ombuf;

		/* copy back to the user space */
		if (copy_to_user(buf, IOMBUF_DATA(mbuf), count)==0)
			r = count;
	}
	/* no matter what, put it back to the free pool */
	ENQUEUE_CONTEXT(global.free_ctxq,qctx);
	ictx->pkt_num++;
	return r;
}

static __inline__ int
mxhw_crypto_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	IOCTRL	*ictx = (IOCTRL*)filp->private_data;
	CIPHER	*info = &ictx->info;
	int		r=0;

	/* register a context and get a control id */
	if (cmd==IOCTLSET_MXCIPHER_INFO)
	{
		if (copy_from_user(info, (void *)arg, sizeof(CIPHER)) || 
			info->algo>=MXCIPHER_ALGO_END || info->mode>=MXCIPHER_MODE_END ||
			mxhw_crypto_engine_register(info, &ictx->cfid)!=0)
				r=-EFAULT;
		else if (info->mode==MXCIPHER_MODE_OFB || info->mode==MXCIPHER_MODE_CTR)
			info->type=1;
	}
	else if(cmd==99)
	{
		*(u_int*)arg = ictx->pkt_num;
	}
#ifdef OVERFLOW_TEST
	else if(cmd==100)
	{
		u_int n;
		QUEUE_LENGTH("ioctl", global.free_ctxq, n);
		*(u_int*)arg = n;
	}
#endif
	else
		r=-EINVAL;
	return r;
}

/* we don't really open a file, instead we start a session */
static int
mxhw_crypto_open(struct inode *inode, struct file * filp)
{
	(void) inode;

	/* check in an available io caller */
	if ((filp->private_data=mxhw_crypto_iocall_chkin(0))==NULL)
		return -EAGAIN;
	MOD_INC_USE_COUNT;
	return 0;
}

static int
mxhw_crypto_close(struct inode *inode, struct file *filp)
{
	IOCTRL *ictx = (IOCTRL*)filp->private_data;

	(void) inode;

	/* for the engine part, close this context */
	mxhw_crypto_engine_unregister(ictx->cfid);
	/* release this io caller and make it available to others */
	mxhw_crypto_iocall_close(ictx->cpid);
	filp->private_data = NULL;
	MOD_DEC_USE_COUNT;
	return 0;
}

static u32 
mxhw_crypto_poll(struct file *filp, poll_table *wait)
{
	IOCTRL	*ictx = (IOCTRL*) filp->private_data;
	u32	mask=0;

	/* waiting for wrtieable */
	poll_wait(filp, &global.free_ctxq->quew, wait);

	if (ictx->outq && ictx->outq->head) mask |= POLLIN | POLLRDNORM;
	if (global.free_ctxq->head) mask |= POLLOUT | POLLWRNORM;
	return mask;
}

static struct file_operations crypto_fops = {
	owner:		THIS_MODULE,
	read:		mxhw_crypto_read,
	write:		mxhw_crypto_write,
	poll:		mxhw_crypto_poll,
	ioctl:		mxhw_crypto_ioctl,
	open:		mxhw_crypto_open,
	release:	mxhw_crypto_close,
};

/* a thread such that it can be put into waiting queue */
static int
mxhw_crypto_dispatch(void *base)
{
	QMBCTX	*qctx;
	IOCTRL	*ictx;
	QMBPTR	*qptr;
	
	/* This thread doesn't need any user-level access, so get rid of all our resources */
	lock_kernel();
	daemonize();
	strcpy(current->comm, "mxcrypto_dispatcher");
	unlock_kernel();
	reparent_to_init();
	/* dispatch loop */
	while(global.dspt_exit==0)
	{
		DBG(KERN_INFO "mxhw_driver: mxhw_crypto_dispatch pid %d\n", current->pid);

		/* requests are dequeued only by the dispatcher, so peek the queue,	if no item,
			the dispatcher goes to sleep until an io caller wakes it up on behalf of
			a request */
		DEQUEUE_CONTEXT(global.dspt_ctxq, qctx, 0, global.dspt_exit);
		if (qctx==NULL)
		{
			/* in case of system reboot */
			if (signal_pending(current))
				break;
			continue;
		}
		/* do it */
		qctx->status = mxhw_crypto_engine_perform(
						qctx->ictx.cfid,
						&qctx->ictx.info,
						&qctx->imbuf,
						&qctx->ombuf,
						&global.dspt_exit);
		/* check alive? */
		ictx = mxhw_crypto_iocall_alive(qctx->ictx.cpid);
		
		/*	a packet has been processed, one of the two, 
			1. enqueue it to its caller's output list, wake up the caller, it is the demand 
				of the caller to pick up this packet
			2. for some reason, the caller might be gone and no place to enqueue 
		*/
		qptr = (ictx)? ictx->outq:global.free_ctxq;

		/* this is a macro */
		ENQUEUE_CONTEXT(qptr, qctx);
		
	}
	complete_and_exit(&dispatcher_exited, 0);
	return 0;
}

#ifndef REGISTER_DEV
static struct miscdevice crypto_miscdev =
{
	CRYPTO_MINOR,
	CRYPTO_DEVNAME,
	&crypto_fops
};
#endif

static void 
mxhw_crypto_global_free(void)
{
	mxhw_crypto_iocall_freeq(0);
	if (global.dspt_ctxq) mxhw_crypto_qmptr_free(global.dspt_ctxq);
	if (global.free_ctxq) mxhw_crypto_qmptr_free(global.free_ctxq);
}

/*
 * The driver boot-time initialization code!
 */
static int __init
mxhw_crypto_init(void)
{
	int num=0;
	
	memset(&global, 0, sizeof(global_p));
	/* hook a device file */
#ifdef REGISTER_DEV
	global.major_num = CRYPTO_MAJOR;
	if ((num = register_chrdev(CRYPTO_MAJOR, CRYPTO_DEVNAME, &crypto_fops))<0)
	{
		printk(KERN_ERR "Can't register char device at %s %d\n", 
				CRYPTO_DEVNAME, global.major_num);
		return -EIO;
	}
	global.major_num = (CRYPTO_MAJOR==0)? num:CRYPTO_MAJOR;
#else
	global.major_num = 10;
	if ((num = misc_register(&crypto_miscdev)))
	{
		printk(KERN_ERR "Can't register misc device at %s %d\n", 
				CRYPTO_DEVNAME, global.major_num);
		return -EIO;
	}
#endif
	
	/* for a plug in engine to start up some procedures */
	if (mxhw_crypto_engine_up()!=0)
	{
		printk("Fail to bring up engine\n");
		return -EFAULT;
	}
	/*	a list of free context buffers (pre-allocated)
		dequeue: 
			1. all io callers compete with 
			2. lock and wait
		enqueue: 
			1. an io caller returns, fails to write/close, etc. 
			2. lock and wake up any other 
	*/
	global.free_ctxq = mxhw_crypto_qmptr_init(MAX_CIPHER_REQUESTS,MAX_CIPHER_PACKET); 
	/*	a list of un-processed requests (empty initially) 
		dequeue: 
			1. the dispatcher gets a request context one-by-one. 
			2. lock and wait
		enqueue: 
			1. any io caller makes a request 
			2. lock and wake up the dispatcher 
	*/	
	global.dspt_ctxq = mxhw_crypto_qmptr_init(0,0);
	if (!global.free_ctxq || !global.dspt_ctxq)
	{
		printk("Fail to mem allocation\n");
		goto err;
	}
	/* finally, create a thread that dispatches requests */
	global.dspt_thrd = kernel_thread(mxhw_crypto_dispatch, NULL, CLONE_SIGHAND);
	if (global.dspt_thrd<0)
	{
		printk("Fail to create the dispatcher\n");
		goto err;
	}
	printk(KERN_INFO "(C)2004-2005 Moxa Inc. Crypto Driver at /dev/%s %d\n",
	       CRYPTO_DEVNAME, global.major_num);
	return 0; 
err:
	mxhw_crypto_global_free();
	return 1;
}

static void __exit
mxhw_crypto_cleanup(void)
{
	printk(KERN_INFO "Unloading crypto module\n");
	/* notify the thread of an exit */
	global.dspt_exit = 1;
	/* the thread might sleep on waiting for requests */
	WAKE_UP_QUEUE(global.dspt_ctxq);
	/* wait until the thread jumps out its loop */
	wait_for_completion(&dispatcher_exited);
	/* shut down the engine */
	mxhw_crypto_engine_down();
	/* free up queues */
	mxhw_crypto_global_free();
	/* unhook the device file */
#ifdef REGISTER_DEV
	unregister_chrdev(global.major_num, CRYPTO_DEVNAME);
#else
	misc_deregister(&crypto_miscdev);
#endif
	printk(KERN_INFO "Crypto module unloaded\n");
}

module_init(mxhw_crypto_init);
module_exit(mxhw_crypto_cleanup);
MODULE_LICENSE("GPL");



