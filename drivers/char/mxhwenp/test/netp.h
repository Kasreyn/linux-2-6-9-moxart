#if defined(WIN32)
#include <windows.h>
#include <winsock.h>
#else
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif /* WIN32 */
#include <sys/types.h>
#include <ctype.h>
#include <stdlib.h>

#if defined(WIN32)
typedef unsigned int socklen_t;
typedef long ssize_t;
#define read(a,b,c) recv(a,b,c,0)
#define write(a,b,c) send(a,b,c,0)
#define close closesocket
#endif /* WIN32 */

#define SERVER_PORT 51234
#define MAX_LEN 2048

