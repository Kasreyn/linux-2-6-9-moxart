# ----------- definition for test programs ----------------------------

OPENSSLDIR = /opt/hardhat/devkit/arm/openssl-0.9.7e

SSLCFLAGS = -I$(OPENSSLDIR)/crypto -I$(OPENSSLDIR)/include -I$(OPENSSLDIR)

ifdef DLL
LDFLAGS += -L$(OPENSSLDIR) -lcrypto
else
SSLAESDIR = $(OPENSSLDIR)/crypto/aes
SSLDESDIR = $(OPENSSLDIR)/crypto/des

SSLSRCS :=
SSLOBJS :=
SSLSRCS += $(SSLDESDIR)/set_key.c $(SSLDESDIR)/des_enc.c $(SSLDESDIR)/ecb_enc.c 
SSLOBJS += $(SSLDESDIR)/set_key.o $(SSLDESDIR)/des_enc.o $(SSLDESDIR)/ecb_enc.o 
SSLSRCS += $(SSLDESDIR)/ecb3_enc.c 
SSLOBJS += $(SSLDESDIR)/ecb3_enc.o 

ifdef CPE
SSLSRCS += $(SSLDESDIR)/ofb64enc.c $(SSLDESDIR)/ofb64ede.c $(SSLDESDIR)/ofb_enc.c
SSLOBJS += $(SSLDESDIR)/ofb64enc.o $(SSLDESDIR)/ofb64ede.o $(SSLDESDIR)/ofb_enc.o
SSLSRCS += $(SSLDESDIR)/cfb64enc.c $(SSLDESDIR)/cfb64ede.c $(SSLDESDIR)/cfb_enc.c
SSLOBJS += $(SSLDESDIR)/cfb64enc.o $(SSLDESDIR)/cfb64ede.o $(SSLDESDIR)/cfb_enc.o
endif

SSLSRCS += $(SSLAESDIR)/aes_core.c $(SSLAESDIR)/aes_ecb.c $(SSLAESDIR)/aes_cbc.c $(SSLAESDIR)/aes_ctr.c 
SSLOBJS += $(SSLAESDIR)/aes_core.o $(SSLAESDIR)/aes_ecb.o $(SSLAESDIR)/aes_cbc.o $(SSLAESDIR)/aes_ctr.o 
ifdef CPE
SSLSRCS += $(SSLAESDIR)/aes_cfb.c $(SSLAESDIR)/aes_ofb.c
SSLOBJS += $(SSLAESDIR)/aes_cfb.o $(SSLAESDIR)/aes_ofb.o
endif

endif
