/*
 *	History:
 *	Date		Author			Comment
 *	11-15-2005	Victor Yu.		Create it.
 */

#include <linux/config.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>

#include <linux/errno.h>
#include <linux/init.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/reboot.h>

#include <asm/hardware.h>
#include <asm/io.h>

static struct map_info mcpu_map_flash = {
	.name		= "W345",
	.bankwidth	= 2,
	.phys		= CPE_FLASH_BASE,
	.size		= CPE_FLASH_SZ,
	.virt		= CPE_FLASH_VA_BASE,
};

#define BOOT_LOADER_SIZE	0x40000
#define KERNEL_BOOT_LOADER_SIZE	0x200000
#define KERNEL_SIZE		(KERNEL_BOOT_LOADER_SIZE-BOOT_LOADER_SIZE)
#define ROOT_DISK_SIZE		0x800000
#define USER_DISK_SIZE		(CPE_FLASH_SZ-KERNEL_BOOT_LOADER_SIZE-ROOT_DISK_SIZE)
static struct mtd_partition mcpu_flash_partitions[] = {
	{
		.name =		"BootLoader",
		.size =		BOOT_LOADER_SIZE,/* hopefully Moxa boot will stay 128k + 128*/
		.offset =	0,
		//.mask_flags =	MTD_WRITEABLE,  /* force read-only */
	}, {
		.name =		"Kernel",
		.size =		KERNEL_SIZE,
		.offset =	MTDPART_OFS_APPEND,
		//.mask_flags =	MTD_WRITEABLE,  /* force read-only */
	}, {
		.name =		"RootDisk",
		.size =		ROOT_DISK_SIZE,
		.offset =	MTDPART_OFS_APPEND,
		//.mask_flags =	MTD_WRITEABLE,  /* force read-only */
	}, {
		.name =		"UserDisk",
		.size =		USER_DISK_SIZE,
		.offset = 	MTDPART_OFS_APPEND,
		//.mask_flags =	MTD_WRITEABLE,
	}
};

static int mtd_reboot(struct notifier_block *n, unsigned long code, void *p)
{
        if(code != SYS_RESTART)
                return NOTIFY_DONE;

	//Added by Wade ,06/19/09  
	*( u16 *)(CPE_FLASH_VA_BASE + (0x55 * 2)) = 0xb0;
	*( u16 *)(CPE_FLASH_VA_BASE + (0x55 * 2)) = 0xff;
        return NOTIFY_DONE;
}

static struct notifier_block mtd_notifier = {
	notifier_call:  mtd_reboot,
	next:           NULL,
	priority:       0
};

static struct mtd_info *flash_mtd;
 
static int __init init_flash (void)   
{

	/*
	 * Static partition definition selection
	 */
	simple_map_init(&mcpu_map_flash);

	/*
	 * Now let's probe for the actual flash.  Do it here since
	 * specific machine settings might have been set above.
	 */
	printk(KERN_NOTICE "Moxa CPU flash: probing %d-bit flash bus\n",
		mcpu_map_flash.bankwidth*8);
	flash_mtd = do_map_probe("cfi_probe", &mcpu_map_flash);
	if (!flash_mtd)
		return -ENXIO;
 
	printk(KERN_NOTICE "Using static partition definition\n");
	return add_mtd_partitions(flash_mtd, mcpu_flash_partitions, ARRAY_SIZE(mcpu_flash_partitions));
}
 
int __init mcpu_mtd_init(void)  
{
	int status;

 	if ((status = init_flash())) {
		printk(KERN_ERR "Flash: unable to init map for flash\n");
	} else {
		register_reboot_notifier(&mtd_notifier);
	}
	return status;
}

static void  __exit mcpu_mtd_cleanup(void)  
{
	if (flash_mtd) {
		unregister_reboot_notifier(&mtd_notifier);
		del_mtd_partitions(flash_mtd);
		map_destroy(flash_mtd);
	}
}

module_init(mcpu_mtd_init);
module_exit(mcpu_mtd_cleanup);

MODULE_AUTHOR("Victor Yu.");
MODULE_DESCRIPTION("Moxa CPU board MTD device driver");
MODULE_LICENSE("GPL");
