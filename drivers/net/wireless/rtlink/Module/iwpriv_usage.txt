iwpriv
=================
This is detailed explanation of each parameters for iwpriv.
Before reading this document, make sure you already read README.

-------------------------------------------------------------------------------------------------------
USAGE:
	iwpriv ra0 set [parameters]=[val]

where

[parameters]          [val] range                          explaination
-----------------   -----------------------              ---------------------------------------------
CountryRegion       {0~7}                                  Set country region 
                                                           0: use 1 ~ 11 Channel
                                                           1: use 1 ~ 11 Channel
                                                           2: use 1 ~ 13 Channel
                                                           3: use 10, 11 Channel
                                                           4: use 10 ~ 13 Channel
                                                           5: use 14 Channel
                                                           6: use 1 ~ 14 Channel
                                                           7: use 3 ~ 9 Channel
                                                           
SSID                {0~z, less than 32 characters}         Set SoftAP SSID

WirelessMode        {0~2}				   Set Wireless Mode 
                                                           0:11b/g mixed, 1:11B only

TxRate              {0~12}				   Set TxRate 
                                                           0:Auto, 1:1Mbps, 2:2Mbps, 3:5.5Mbps, 4:11Mbps, 
                                                           5:6Mbps, 6:9Mbps, 7:12Mbps, 8:18Mbps, 9:24Mbps, 
                                                           10:36Mbps, 11:48Mbps, 12:54Mbps
                                                           
Channel             {1~14} depends on country region       Set Channel

BGProtection        {0~2}                                  Set 11B/11G Protection
                                                           0:Auto, 1:Always on, 2:Always off

TxPreamble          {0~2}                                  Set TxPreamble
                                                           0:Preamble Long, 1:Preamble Short, 2:Auto

RTSThreshold        {1~2347}                               Set RTS Threshold                                                           
                                                           
FragThreshold       {256~2346}                             Set Fragment Threshold

TxBurst             {0,1}                                  Set TxBurst Enable or Disable
                                                           0:Disable, 1:Enable

TurboRate           {0,1}                                  Set TurboRate Enable or Disable
                                                           0:Disable, 1:Enable

AdhocModeRate       {0, 1}                                 Set Adhoc mode tx rate
							   0: adhere WIFI spec., 1: violate WIFI spec.
							   (802.11g WIFI spec disallow OFDM rates in 802.11g ADHOC mode)

NetworkType	    {Infra,Adhoc}			   Set Network type
                                                                                                                                                    	                                                          
AuthMode            {OPEN,SHARED,WPAPSK}                   Set Authentication mode

EncrypType          {NONE,WEP,TKIP,AES}                    Set Encryption Type

DefaultKeyID        {1~4}                                  Set Default Key ID

Key1                 {5 ascii characters or                 Set Key1 String
                     10 hex number or 
                     13 ascii characters or 
                     26 hex numbers}                                                                                                                        

Key2                 {5 ascii characters or                 Set Key2 String
                     10 hex number or 
                     13 ascii characters or 
                     26 hex numbers}                                                                                                                        

Key3                 {5 ascii characters or                 Set Key3 String
                     10 hex number or 
                     13 ascii characters or 
                     26 hex numbers}                                                                                                                        

Key4                 {5 ascii characters or                 Set Key4 String
                     10 hex number or 
                     13 ascii characters or 
                     26 hex numbers}                                                                                                                        

                                                          
WPAPSK              {8~63 ASCII or 64 HEX characters}       WPA Pre-Shared Key 

ApClient	    {0,1}				    Set ApClient mode
							    0:Disable, 1:Enable


Examples
===================================================================
-------------------------------------------------------------------------------------------------------
Example I: Config STA to link with AP which is OPEN/NONE(Authentication/Encryption)
	1. iwpriv ra0 set NetworkType=Infra
	2. iwpriv ra0 set AuthMode=OPEN
	3. iwpriv ra0 set EncrypType=NONE
	4. iwpriv ra0 set SSID="AP's SSID"
	
Example II: Config STA to link with AP which is SHARED/WEP(Authentication/Encryption)
	1. iwpriv ra0 set NetworkType=Infra
	2. iwpriv ra0 set AuthMode=SHARED
	3. iwpriv ra0 set EncrypType=WEP
	4. iwpriv ra0 set Key1="AP's wep key"
	5. iwpriv ra0 set DefaultKeyID=1
	6. iwpriv ra0 set SSID="AP's SSID"
	
Example III: Config STA to link with AP which is WPAPSK/TKIP(Authentication/Encryption)
	1. iwpriv ra0 set NetworkType=Infra
	2. iwpriv ra0 set AuthMode=WPAPSK
	3. iwpriv ra0 set EncrypType=TKIP
	4. iwpriv ra0 set SSID="AP's SSID"
	5. iwpriv ra0 set WPAPSK="AP's wpa-preshared key"
	6. iwpriv ra0 set SSID="AP's SSID"
	
Example IV: Config STA to link with AP which is WPAPSK/AES(Authentication/Encryption)
	1. iwpriv ra0 set NetworkType=Infra
	2. iwpriv ra0 set AuthMode=WPAPSK
	3. iwpriv ra0 set EncrypType=AES
	4. iwpriv ra0 set SSID="AP's SSID"
	5. iwpriv ra0 set WPAPSK="AP's wpa-preshared key"
	6. iwpriv ra0 set SSID="AP's SSID"
	
Example V: Config STA to create/link as adhoc mode
	1. iwpriv ra0 set NetworkType=Adhoc
	2. iwpriv ra0 set AuthMode=OPEN
	3. iwpriv ra0 set EncrypType=NONE
	4. iwpriv ra0 set SSID="Adhoc's SSID"



iwlist
=================
This is detailed explanation of each parameters for iwlist.

-------------------------------------------------------------------------------------------------------

iwlist ra0 scanning		; list the result after scanning(site survey)

