/*************************************************************************
 * Ralink Tech Inc.                                                      *
 * 4F, No. 2 Technology 5th Rd.                                          *
 * Science-based Industrial Park                                         *
 * Hsin-chu, Taiwan, R.O.C.                                              *
 *                                                                       *
 * (c) Copyright 2002, Ralink Technology, Inc.                           *
 *                                                                       *
 * This program is free software; you can redistribute it and/or modify  * 
 * it under the terms of the GNU General Public License as published by  * 
 * the Free Software Foundation; either version 2 of the License, or     * 
 * (at your option) any later version.                                   * 
 *                                                                       * 
 * This program is distributed in the hope that it will be useful,       * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         * 
 * GNU General Public License for more details.                          * 
 *                                                                       * 
 * You should have received a copy of the GNU General Public License     * 
 * along with this program; if not, write to the                         * 
 * Free Software Foundation, Inc.,                                       * 
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             * 
 *                                                                       * 
 *************************************************************************

    Module Name:
    rt_config.h

    Abstract:
    Central header file to maintain all include files for all NDIS
    miniport driver routines.

    Revision History:
    Who         When          What
    --------    ----------    ----------------------------------------------
    Rory Chen   12-21-2002    created

*/

#ifndef __RT_CONFIG_H__
#define __RT_CONFIG_H__

#define PROFILE_PATH                "/etc/Wireless/RT2500STA/RT2500STA.dat"
#define NIC_DEVICE_NAME             "RT2500STA"

/* Version */
#define DRV_MAJORVERSION		1
#define DRV_MINORVERSION		4
#define DRV_SUBVERSION	        6	
#define DRV_TESTVERSION	        6	
#define DRV_YEAR		        2006
#define DRV_MONTH		        9
#define DRV_DAY		            13

/* Operational parameters that are set at compile time. */
#if !defined(__OPTIMIZE__)  ||  !defined(__KERNEL__)
#warning  You must compile this file with the correct options!
#warning  See the last lines of the source file.
#error  You must compile this driver with "-O".
#endif

#include <linux/config.h>  //can delete
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/timer.h>
#include <linux/errno.h> //can delete
#include <linux/ioport.h> // can delete
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/init.h>  //can delete
#include <linux/delay.h> // can delete
#include <linux/ethtool.h>
#include <linux/wireless.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/if_arp.h>
#include <linux/ctype.h>

#if LINUX_VERSION_CODE >= 0x20407
#include <linux/mii.h>
#endif
#include <asm/processor.h>      /* Processor type for cache alignment. */
#include <asm/bitops.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>

// The type definition has to be placed before including rt2460.h
#ifndef ULONG
#define CHAR            char
#define INT             int
#define SHORT             int
#define UINT            u32
#define ULONG           u32
#define USHORT          u16
#define UCHAR           u8

#define BOOLEAN         u8
//#define LARGE_INTEGER s64
#define VOID            void
#define LONG            int
#define ULONGLONG       u64
typedef VOID            *PVOID;
typedef CHAR            *PCHAR;
typedef UCHAR           *PUCHAR;
typedef LONG            *PLONG;
typedef ULONG           *PULONG;

typedef union _LARGE_INTEGER {
    struct {
        ULONG LowPart;
        LONG HighPart;
    }vv;
    struct {
        ULONG LowPart;
        LONG HighPart;
    } u;
    s64 QuadPart;
} LARGE_INTEGER;

#endif

#define IN
#define OUT

#define TRUE        1
#define FALSE       0

#define ETH_LENGTH_OF_ADDRESS   6

#define NDIS_STATUS                             INT
#define NDIS_STATUS_SUCCESS                     0x00
#define NDIS_STATUS_FAILURE                     0x01
#define NDIS_STATUS_RESOURCES                   0x03
#define NDIS_STATUS_MEDIA_DISCONNECT            0x04
#define NDIS_STATUS_MEDIA_CONNECT               0x05

#include    "rtmp_type.h"
#include    "rtmp_def.h"
#include    "rt2560.h"
#include    "rtmp.h"
#include    "mlme.h"
#include    "oid.h"
#include    "wpa.h"
#include    "md5.h"

#define DEBUG_TASK_DELAY        2000

enum rt2560_chips {
    RT2560A = 0,
};

#if 1	//#ifdef RTMP_EMBEDDED
#undef GFP_KERNEL
#define GFP_KERNEL      (GFP_DMA | GFP_ATOMIC)
#endif

#endif  // __RT_CONFIG_H__
