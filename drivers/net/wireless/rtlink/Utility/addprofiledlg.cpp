#include "addprofiledlg.h"

#include <qapplication.h>
#include <qheader.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qvariant.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qslider.h>
#include <qspinbox.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qvalidator.h>
#include <qmessagebox.h>
#include <qlistview.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/wireless.h>
#include <ctype.h>

#include "rt_tool.h"

extern bool                         G_bSupportAMode;
extern RT_802_11_PHY_MODE           G_enumWirelessMode;
extern PAIR_CHANNEL_FREQ_ENTRY      ChannelFreqTable[];
extern int                          G_nChanFreqCount;
extern UINT                         G_nCountryRegion;
extern const char *strAryNetworkType[];

// 2.4 Ghz channel plan
static UCHAR                        Ra24Ghz_FCC[] = {1,2,3,4,5,6,7,8,9,10,11};
static UCHAR                        Ra24Ghz_IC[] = {1,2,3,4,5,6,7,8,9,10,11};
static UCHAR                        Ra24Ghz_ESTI[]= {1,2,3,4,5,6,7,8,9,10,11,12,13};
static UCHAR                        Ra24Ghz_SPAIN[] = {10,11};
static UCHAR                        Ra24Ghz_FRANCE[] = {10,11,12,13};
static UCHAR                        Ra24Ghz_MKK[] = {14};
static UCHAR                        Ra24Ghz_MKK1[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
static UCHAR                        Ra24Ghz_ISRAEL[] = {3,4,5,6,7,8,9};
// 5 Ghz channel plan
static UCHAR                        Ra5Ghz_UNII[] = {36,40,44,48,52,56,60,64,  149,153,157,161};
//static UCHAR                        Ra5Ghz_MMAC[] = {34,38,42,46};
//static UCHAR                        Ra5Ghz_HyperLAN2[] = {36,40,44,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136,140};

static QPixmap uic_load_pixmap_AddProfileDlg( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
        return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}
/* 
 *  Constructs a AddProfileDlg which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
AddProfileDlg::AddProfileDlg( int socket_id, const char *device_name, QWidget* parent,  const char* name, int Type, QListView* pListView, PRT_PROFILE_SETTING *pProfilePtr, NDIS_802_11_MAC_ADDRESS *pBSsid, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
        setName( "AddProfileDlg" );
    resize( 620, 420 );
    setMinimumSize( 620, 420 );
    setMaximumSize( 620, 420 );
    setCaption("AddProfile");
    qApp->setStyle("Windows");

    TextLabel1 = new QLabel( this, "TextLabel1" );
    TextLabel1->setGeometry( QRect( 30, 15, 90, 30 ) );
    TextLabel1->setText("Profile Name");

    LineEdit_ProfileName = new QLineEdit( this, "LineEdit_ProfileName" );
    LineEdit_ProfileName->setGeometry( QRect( 140, 15, 200, 30 ) ); 

    TextLabel2 = new QLabel( this, "TextLabel2" );
    TextLabel2->setGeometry( QRect( 360, 15, 50, 30 ) ); 
    TextLabel2->setText("SSID");

    ComboBox_SSID = new QComboBox( FALSE, this, "ComboBox_SSID" );
    ComboBox_SSID->setGeometry( QRect( 410, 15, 200, 30 ) ); 
    ComboBox_SSID->setEditable( TRUE );

    TabWidget_Config = new QTabWidget( this, "TabWidget_Config" );
    TabWidget_Config->setGeometry( QRect( 20, 50, 580, 320 ) ); 

    ConfigPage_tab = new QWidget( TabWidget_Config, "ConfigPage_tab" );

    Config_ButtonGroup_PowerSaveing = new QButtonGroup( ConfigPage_tab, "Config_ButtonGroup_PowerSaveing" );
    Config_ButtonGroup_PowerSaveing->setGeometry( QRect( 20, 10, 535, 60 ) ); 
    Config_ButtonGroup_PowerSaveing->setTitle("Power Saving Mode");
    Config_ButtonGroup_PowerSaveing->setExclusive( FALSE );

    Config_RadioButton_CAM = new QRadioButton( Config_ButtonGroup_PowerSaveing, "Config_RadioButton_CAM" );
    Config_RadioButton_CAM->setEnabled( TRUE );
    Config_RadioButton_CAM->setGeometry( QRect( 25, 20, 200, 30 ) ); 
    Config_RadioButton_CAM->setText("CAM (Constantly Awake Mode)");
    Config_RadioButton_CAM->setChecked( TRUE );
    Config_ButtonGroup_PowerSaveing->insert( Config_RadioButton_CAM, 0 );

    Config_RadioButton_PSMode = new QRadioButton( Config_ButtonGroup_PowerSaveing, "Config_RadioButton_PSMode" );
    Config_RadioButton_PSMode->setEnabled( TRUE );
    Config_RadioButton_PSMode->setGeometry( QRect( 300, 20, 200, 30 ) ); 
    Config_RadioButton_PSMode->setText("Power Saving Mode");
    Config_RadioButton_PSMode->setChecked( FALSE );
    Config_ButtonGroup_PowerSaveing->insert( Config_RadioButton_PSMode, 1 );

    Config_TextLabel1 = new QLabel( ConfigPage_tab, "Config_TextLabel1" );
    Config_TextLabel1->setGeometry( QRect( 25, 85, 100, 30 ) ); 
    Config_TextLabel1->setText("Network Type");

    Config_ComboBox_NetworkType = new QComboBox( FALSE, ConfigPage_tab, "Config_ComboBox_NetworkType" );
    Config_ComboBox_NetworkType->insertItem("802.11 Ad Hoc");
    Config_ComboBox_NetworkType->insertItem("Infrasturcture");
    Config_ComboBox_NetworkType->setGeometry( QRect( 150, 85, 180, 30 ) ); 
    Config_ComboBox_NetworkType->setCurrentItem( 1 );

    Config_TextLabel2 = new QLabel( ConfigPage_tab, "Config_TextLabel2" );
    Config_TextLabel2->setGeometry( QRect( 25, 130, 120, 30 ) ); 
    Config_TextLabel2->setText("11B Preamble Type");

    Config_ComboBox_Preamble = new QComboBox( FALSE, ConfigPage_tab, "Config_ComboBox_Preamble" );
    Config_ComboBox_Preamble->insertItem("Auto");
    Config_ComboBox_Preamble->insertItem("Long");
    Config_ComboBox_Preamble->setEnabled( FALSE );
    Config_ComboBox_Preamble->setGeometry( QRect( 150, 130, 180, 30 ) ); 

    Config_CheckBox_RTS = new QCheckBox( ConfigPage_tab, "Config_CheckBox_RTS" );
    Config_CheckBox_RTS->setGeometry( QRect( 15, 187, 130, 30 ) ); 
    Config_CheckBox_RTS->setText("RTS Threshold");

    Config_TextLabel3 = new QLabel( ConfigPage_tab, "Config_TextLabel3" );
    Config_TextLabel3->setGeometry( QRect( 165, 167, 20, 30 ) ); 
    Config_TextLabel3->setText("0");

    Config_Slider_RTS = new QSlider( ConfigPage_tab, "Config_Slider_RTS" );
    Config_Slider_RTS->setEnabled( FALSE );
    Config_Slider_RTS->setGeometry( QRect( 195, 187, 180, 30 ) ); 
    Config_Slider_RTS->setMaxValue( 2312 );
    Config_Slider_RTS->setValue( 2312 );
    Config_Slider_RTS->setOrientation( QSlider::Horizontal );

    Config_TextLabel4 = new QLabel( ConfigPage_tab, "Config_TextLabel4" );
    Config_TextLabel4->setGeometry( QRect( 385, 167, 35, 30 ) ); 
    Config_TextLabel4->setText("2312");

    Config_SpinBox_RTS = new QSpinBox( ConfigPage_tab, "Config_SpinBox_RTS" );
    Config_SpinBox_RTS->setEnabled( FALSE );
    Config_SpinBox_RTS->setGeometry( QRect( 425, 187, 50, 30 ) ); 
    Config_SpinBox_RTS->setMaxValue( 2312 );
    Config_SpinBox_RTS->setValue( 2312 );

    Config_CheckBox_Fragment = new QCheckBox( ConfigPage_tab, "Config_CheckBox_Fragment" );
    Config_CheckBox_Fragment->setGeometry( QRect( 15, 220, 140, 30 ) ); 
    Config_CheckBox_Fragment->setText("Fragment Threshold");

    Config_Slider_Fragment = new QSlider( ConfigPage_tab, "Config_Slider_Fragment" );
    Config_Slider_Fragment->setEnabled( FALSE );
    Config_Slider_Fragment->setGeometry( QRect( 195, 225, 180, 30 ) ); 
    Config_Slider_Fragment->setMinValue( 256 );
    Config_Slider_Fragment->setMaxValue( 2312 );
    Config_Slider_Fragment->setValue( 2312 );
    Config_Slider_Fragment->setOrientation( QSlider::Horizontal );

    Config_TextLabel5 = new QLabel( ConfigPage_tab, "Config_TextLabel5" );
    Config_TextLabel5->setGeometry( QRect( 165, 205, 20, 30 ) ); 
    Config_TextLabel5->setText("256");

    Config_SpinBox_Fragment = new QSpinBox( ConfigPage_tab, "Config_SpinBox_Fragment" );
    Config_SpinBox_Fragment->setEnabled( FALSE );
    Config_SpinBox_Fragment->setGeometry( QRect( 425, 225, 50, 30 ) ); 
    Config_SpinBox_Fragment->setMaxValue( 2312 );
    Config_SpinBox_Fragment->setMinValue( 256 );
    Config_SpinBox_Fragment->setValue( 2312 );

    Config_TextLabel6 = new QLabel( ConfigPage_tab, "Config_TextLabel6" );
    Config_TextLabel6->setGeometry( QRect( 385, 205, 35, 30 ) ); 
    Config_TextLabel6->setText("2312");

    Config_TextLabel_Channel = new QLabel( ConfigPage_tab, "Config_TextLabel_Channel" );
    Config_TextLabel_Channel->setGeometry( QRect( 490, 187, 61, 31 ) ); 
    Config_TextLabel_Channel->setText("Channel");
    Config_TextLabel_Channel->hide();

    Config_ComboBox_Channel = new QComboBox( FALSE, ConfigPage_tab, "Config_ComboBox_Channel" );
    Config_ComboBox_Channel->insertItem("1");
    Config_ComboBox_Channel->setGeometry( QRect( 490, 225, 71, 31 ) );
    Config_ComboBox_Channel->hide();

    TabWidget_Config->insertTab( ConfigPage_tab, "System Configuration");

//Authentication_Security Page
    SecurityPage_tab = new QWidget( TabWidget_Config, "SecurityPage_tab" );

    Security_TextLabel1 = new QLabel( SecurityPage_tab, "Security_TextLabel1" );
    Security_TextLabel1->setGeometry( QRect( 15, 15, 120, 30 ) ); 
    Security_TextLabel1->setText("Authentication Type:");

    Security_ComboBox_AuthType = new QComboBox( FALSE, SecurityPage_tab, "Security_ComboBox_AuthType" );
    Security_ComboBox_AuthType->insertItem("OPEN");
    Security_ComboBox_AuthType->insertItem("SHARED");
    Security_ComboBox_AuthType->insertItem("WPAPSK");
    Security_ComboBox_AuthType->setGeometry( QRect( 145, 15, 120, 30 ) );
    Security_ComboBox_AuthType->setCurrentItem(0);

    Security_TextLabel2 = new QLabel( SecurityPage_tab, "Security_TextLabel2" );
    Security_TextLabel2->setGeometry( QRect( 308, 15, 120, 30 ) ); 
    Security_TextLabel2->setText("Encryptiion Type:");

    Security_ComboBox_Encrypt = new QComboBox( FALSE, SecurityPage_tab, "Security_ComboBox_Encrypt" );
    Security_ComboBox_Encrypt->insertItem("NONE");
    Security_ComboBox_Encrypt->insertItem("WEP");
    Security_ComboBox_Encrypt->setGeometry( QRect( 435, 16, 120, 30 ) );
    Security_ComboBox_Encrypt->setCurrentItem(0);

    Security_TextLabel3 = new QLabel( SecurityPage_tab, "Security_TextLabel3" );
    Security_TextLabel3->setGeometry( QRect( 15, 60, 120, 31 ) ); 
    Security_TextLabel3->setText("WPA Pre-Shared Key:");

    Security_LineEdit_PSK = new QLineEdit( SecurityPage_tab, "Security_LineEdit_PSK" );
    Security_LineEdit_PSK->setGeometry( QRect( 145, 60, 410, 30 ) ); 
    Security_LineEdit_PSK->setMaxLength(64);
    Security_LineEdit_PSK->setEnabled( FALSE );

    Security_ButtonGroup_Key = new QButtonGroup( SecurityPage_tab, "Security_ButtonGroup_Key" );
    Security_ButtonGroup_Key->setGeometry( QRect( 20, 108, 535, 170 ) ); 
    Security_ButtonGroup_Key->setTitle("WEP Key");

    Security_RadioButton_Key1 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key1" );
    Security_RadioButton_Key1->setGeometry( QRect( 10, 20, 70, 30 ) ); 
    Security_RadioButton_Key1->setText("Key#1");
    Security_RadioButton_Key1->setEnabled( FALSE );
    Security_RadioButton_Key1->setChecked( TRUE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key1, 0 );

    Security_ComboBox_KeyType1 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType1" );
    Security_ComboBox_KeyType1->insertItem("Hexadecimal");
    Security_ComboBox_KeyType1->insertItem("Ascii");
    Security_ComboBox_KeyType1->setGeometry( QRect( 90, 20, 120, 30 ) ); 
    Security_ComboBox_KeyType1->setEnabled( FALSE );

    m_hexValidator = new QHexValidator(this);

    Security_LineEdit_Key1Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key1Hex" );
    Security_LineEdit_Key1Hex->setGeometry( QRect( 225, 20, 290, 30 ) );
    Security_LineEdit_Key1Hex->setMaxLength(26);
    Security_LineEdit_Key1Hex->setEnabled( FALSE );
    Security_LineEdit_Key1Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key1Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key1Ascii" );
    Security_LineEdit_Key1Ascii->setGeometry( QRect( 225, 20, 290, 30 ) );
    Security_LineEdit_Key1Ascii->setMaxLength(13);
    Security_LineEdit_Key1Ascii->hide();
    Security_LineEdit_Key1Ascii->setEnabled( FALSE );

    Security_RadioButton_Key2 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key2" );
    Security_RadioButton_Key2->setGeometry( QRect( 10, 55, 70, 30 ) ); 
    Security_RadioButton_Key2->setText("Key#2");
    Security_RadioButton_Key2->setEnabled( FALSE );
    Security_RadioButton_Key2->setChecked( FALSE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key2, 1 );

    Security_ComboBox_KeyType2 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType2" );
    Security_ComboBox_KeyType2->insertItem("Hexadecimal");
    Security_ComboBox_KeyType2->insertItem("Ascii");
    Security_ComboBox_KeyType2->setGeometry( QRect( 90, 55, 120, 30 ) );
    Security_ComboBox_KeyType2->setEnabled( FALSE );

    Security_LineEdit_Key2Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key2Hex" );
    Security_LineEdit_Key2Hex->setGeometry( QRect( 225, 55, 290, 30 ) );
    Security_LineEdit_Key2Hex->setMaxLength(26);
    Security_LineEdit_Key2Hex->setEnabled( FALSE );
    Security_LineEdit_Key2Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key2Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key2Ascii" );
    Security_LineEdit_Key2Ascii->setGeometry( QRect( 225, 55, 290, 30 ) );
    Security_LineEdit_Key2Ascii->setMaxLength(13);
    Security_LineEdit_Key2Ascii->hide();
    Security_LineEdit_Key2Ascii->setEnabled( FALSE );

    Security_RadioButton_Key3 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key3" );
    Security_RadioButton_Key3->setGeometry( QRect( 10, 90, 70, 30 ) ); 
    Security_RadioButton_Key3->setText("Key#3");
    Security_RadioButton_Key3->setEnabled( FALSE );
    Security_RadioButton_Key3->setChecked( FALSE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key3, 2 );

    Security_ComboBox_KeyType3 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType3" );
    Security_ComboBox_KeyType3->insertItem("Hexadecimal");
    Security_ComboBox_KeyType3->insertItem("Ascii");
    Security_ComboBox_KeyType3->setGeometry( QRect( 90, 90, 120, 30 ) );
    Security_ComboBox_KeyType3->setEnabled( FALSE );

    Security_LineEdit_Key3Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key3Hex" );
    Security_LineEdit_Key3Hex->setGeometry( QRect( 225, 90, 290, 30 ) );
    Security_LineEdit_Key3Hex->setMaxLength(26);
    Security_LineEdit_Key3Hex->setEnabled( FALSE );
    Security_LineEdit_Key3Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key3Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key3Ascii" );
    Security_LineEdit_Key3Ascii->setGeometry( QRect( 225, 90, 290, 30 ) );
    Security_LineEdit_Key3Ascii->setMaxLength(13);
    Security_LineEdit_Key3Ascii->hide();
    Security_LineEdit_Key3Ascii->setEnabled( FALSE );

    Security_RadioButton_Key4 = new QRadioButton( Security_ButtonGroup_Key, "Security_RadioButton_Key4" );
    Security_RadioButton_Key4->setGeometry( QRect( 10, 125, 70, 30 ) ); 
    Security_RadioButton_Key4->setText("Key#4");
    Security_RadioButton_Key4->setEnabled( FALSE );
    Security_RadioButton_Key4->setChecked( FALSE );
    Security_ButtonGroup_Key->insert( Security_RadioButton_Key4, 3 );

    Security_ComboBox_KeyType4 = new QComboBox( FALSE, Security_ButtonGroup_Key, "Security_ComboBox_KeyType4" );
    Security_ComboBox_KeyType4->insertItem("Hexadecimal");
    Security_ComboBox_KeyType4->insertItem("Ascii");
    Security_ComboBox_KeyType4->setGeometry( QRect( 90, 125, 120, 30 ) );
    Security_ComboBox_KeyType4->setEnabled( FALSE );

    Security_LineEdit_Key4Hex = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key4Hex" );
    Security_LineEdit_Key4Hex->setGeometry( QRect( 225, 125, 290, 30 ) );
    Security_LineEdit_Key4Hex->setMaxLength(26);
    Security_LineEdit_Key4Hex->setEnabled( FALSE );
    Security_LineEdit_Key4Hex->setValidator(m_hexValidator);

    Security_LineEdit_Key4Ascii = new QLineEdit( Security_ButtonGroup_Key, "Security_LineEdit_Key4Ascii" );
    Security_LineEdit_Key4Ascii->setGeometry( QRect( 225, 125, 290, 30 ) );
    Security_LineEdit_Key4Ascii->setEnabled( FALSE );
    Security_LineEdit_Key4Ascii->setMaxLength(13);
    Security_LineEdit_Key4Ascii->hide();
    Security_LineEdit_Key4Ascii->setEnabled( FALSE );

    TabWidget_Config->insertTab( SecurityPage_tab, "Authentication && Security");

    PushButton_OK = new QPushButton( this, "PushButton_OK" );
    PushButton_OK->setGeometry( QRect( 170, 380, 120, 30 ) ); 
    PushButton_OK->setText("&OK");

    PushButton_Cancel = new QPushButton( this, "PushButton_Cancel" );
    PushButton_Cancel->setGeometry( QRect( 340, 380, 120, 30 ) ); 
    PushButton_Cancel->setText("&CANCEL");

    connect( PushButton_OK, SIGNAL( clicked() ), this, SLOT( OnOK() ) );
    connect( PushButton_Cancel, SIGNAL( clicked() ), this, SLOT( OnCancel() ) );
    connect( ComboBox_SSID, SIGNAL( activated(int) ), this, SLOT( Config_OnSelectSSID(int) ) );
    connect( Config_ComboBox_NetworkType, SIGNAL( activated(int) ), this, SLOT( Config_OnSelectNetworkType(int) ) );
    connect( Config_CheckBox_RTS, SIGNAL( toggled(bool) ), this, SLOT( Config_OnEnableRTS(bool) ) );
    connect( Config_CheckBox_Fragment, SIGNAL( toggled(bool) ), this, SLOT( Config_OnEnableFragment(bool) ) );
    connect( Config_Slider_RTS, SIGNAL( valueChanged(int) ), this, SLOT( Config_OnRTSSliderChanged(int) ) );
    connect( Config_Slider_Fragment, SIGNAL( valueChanged(int) ), this, SLOT( Config_OnFragmentSliderChanged(int) ) );
    connect( Config_SpinBox_RTS, SIGNAL( valueChanged(int) ), this, SLOT( Config_OnRTSSpinBoxChanged(int) ) );
    connect( Config_SpinBox_Fragment, SIGNAL( valueChanged(int) ), this, SLOT( Config_OnFragmentSpinBoxChanged(int) ) );
    connect( Security_ComboBox_AuthType, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectAuthenType(int) ) );
    connect( Security_ComboBox_Encrypt, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectEncryptType(int) ) );
    connect( Security_ComboBox_KeyType1, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey1Type(int) ) );
    connect( Security_ComboBox_KeyType2, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey2Type(int) ) );
    connect( Security_ComboBox_KeyType3, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey3Type(int) ) );
    connect( Security_ComboBox_KeyType4, SIGNAL( activated(int) ), this, SLOT( Security_OnSelectKey4Type(int) ) );

    m_IsAddToProfile = FALSE;
    m_pBssidList = (PNDIS_802_11_BSSID_LIST_EX) malloc(65536); //64K
    m_nSocketId = socket_id;

    if (device_name)
    {
        m_strDeviceName =  (char *)malloc(strlen(device_name)+1);
        if (m_strDeviceName)
            strcpy(m_strDeviceName, device_name);
    }

    if (pBSsid)
    {
        memcpy(m_BSsid, pBSsid, sizeof(NDIS_802_11_MAC_ADDRESS));
    }

    m_type = Type;
    m_pProfileSetting = *pProfilePtr;

    if (m_pProfileSetting == NULL)
    {
        m_pProfileSetting = (PRT_PROFILE_SETTING) malloc(sizeof(RT_PROFILE_SETTING));
        memset(m_pProfileSetting, 0x00, sizeof(RT_PROFILE_SETTING));
        *pProfilePtr = m_pProfileSetting;
        m_bIsProfileNull = TRUE;
    }
    else
    {
        m_pProfileSetting = *pProfilePtr;
        m_bIsProfileNull = FALSE;
    }

    if (pListView)
        m_ProfileListView = pListView;
    else
        m_ProfileListView = NULL;

    InitDlg();
}

/*  
 *  Destroys the object and frees any allocated resources
 */
AddProfileDlg::~AddProfileDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

void AddProfileDlg::InitDlg()
{
    QListViewItem                       *Item;
    QString                             qstr;
    PRT_PROFILE_SETTING                 ptr = NULL;
    char                                tmp[40];
    UINT                                i = 0;
    bool                                bfound = FALSE;

    InitChannel();
    GetAllSsid();

    if (m_type == PROFILE_ADD)
    {
        for (i=1; i < 1000; i++)
        {
            sprintf(tmp, "PROF%03d", i);
            ptr = m_pProfileSetting;
            bfound = FALSE;
            while (ptr!=NULL)
            {
                if (strcmp(tmp, (const char *)ptr->Profile) == 0)
                {
                    bfound = TRUE;
                    break;
                }
                ptr=ptr->Next;
            }

            if (!bfound)
            {
                LineEdit_ProfileName->setText(tmp);
                break;
            }

        }
    }
    else if (m_type == PROFILE_EDIT)
    {
        Item = m_ProfileListView->currentItem();
        qstr = Item->text(0);

        ptr = m_pProfileSetting;
        while (ptr!=NULL)
        {
            if (qstr.compare((const char *)ptr->Profile) == 0)
            {
                bfound = TRUE;
                m_pEditProfileSetting = ptr;
                LineEdit_ProfileName->setText((const char *)ptr->Profile);
                ComboBox_SSID->setCurrentText((const char *)ptr->SSID);

                switch (ptr->PSmode)
                {
                case Ndis802_11PowerModeCAM:
                    Config_ButtonGroup_PowerSaveing->setButton(0);
                    break;
                case Ndis802_11PowerModeMAX_PSP:
                    Config_ButtonGroup_PowerSaveing->setButton(1);
                    break;
                case Ndis802_11PowerModeFast_PSP:
                    Config_ButtonGroup_PowerSaveing->setButton(2);
                    break;
                default:
                    break;
                }

                if (ptr->NetworkType == Ndis802_11IBSS)
                {
                    Config_ComboBox_NetworkType->setCurrentItem(0);
                    Config_TextLabel_Channel->show();
                    Config_ComboBox_Channel->show();
                    if (ptr->Channel == 0)
                        Config_ComboBox_Channel->setCurrentItem(0);
                    else
                    {
                        sprintf(tmp, "%d", ptr->Channel);
                        Config_ComboBox_Channel->setCurrentText(tmp);
                    }
                }
                else
                {
                    Config_ComboBox_NetworkType->setCurrentItem(1);
                    Config_TextLabel_Channel->hide();
                    Config_ComboBox_Channel->hide();
                }

                if (ptr->PreamType == Rt802_11PreambleAuto)
                    Config_ComboBox_Preamble->setCurrentItem(0);
                else //long
                    Config_ComboBox_Preamble->setCurrentItem(1);

                Config_CheckBox_RTS->setChecked(ptr->RTSCheck);

                Config_Slider_RTS->setValue( ptr->RTS );

                Config_SpinBox_RTS->setValue( ptr->RTS );

                Config_CheckBox_Fragment->setChecked(ptr->FragmentCheck);

                Config_Slider_Fragment->setValue(ptr->Fragment);

                Config_SpinBox_Fragment->setValue(ptr->Fragment);

                Security_SetAuthModeAndEncryType(ptr->NetworkType, ptr->Authentication, ptr->Encryption);

                Security_LineEdit_PSK->setText(ptr->WpaPsk);
                Security_SetDefaultKeyId(ptr->KeyDefaultId);
                Security_SetKeyTypeAndKeyString(0, ptr->Key1Type, ptr->Key1);
                Security_SetKeyTypeAndKeyString(1, ptr->Key2Type, ptr->Key2);
                Security_SetKeyTypeAndKeyString(2, ptr->Key3Type, ptr->Key3);
                Security_SetKeyTypeAndKeyString(3, ptr->Key4Type, ptr->Key4);
            }
            ptr=ptr->Next;
        }
        if(!bfound)
        {
            QMessageBox::warning(this, "Warning", "Edit Profile failed, try again!");
            close();
        }
    }
}

void AddProfileDlg::GetAllSsid()
{
    PNDIS_WLAN_BSSID_EX                 pBssid;
    QString                             qStrSsid;
    char                                strSSID[NDIS_802_11_LENGTH_SSID + 1];
    ULONG                               lBufLen = 65536; // 64K
    ULONG                               i = 0;
    int                                 nChannel = 1;
    char                                tmp[40];

    if (OidQueryInformation(OID_802_11_BSSID_LIST, m_nSocketId, m_strDeviceName, m_pBssidList, lBufLen) < 0)
        return;  //failed

    ComboBox_SSID->clear();
    ComboBox_SSID->insertItem("");

    pBssid = m_pBssidList->Bssid;

    // Add each SSID to the combo 
    for(i = 0; i < m_pBssidList->NumberOfItems; i++)
    {
        memset(strSSID, 0x00, NDIS_802_11_LENGTH_SSID + 1);
        memcpy(strSSID, pBssid->Ssid.Ssid, pBssid->Ssid.SsidLength);

        ComboBox_SSID->insertItem(strSSID);

        if(memcmp(pBssid->MacAddress, m_BSsid, 6) == 0)
        {
            ComboBox_SSID->setCurrentText(strSSID);
            if (pBssid->InfrastructureMode == Ndis802_11Infrastructure)
            {
                Config_ComboBox_NetworkType->setCurrentItem(1);
                Config_TextLabel_Channel->hide();
                Config_ComboBox_Channel->hide();
            }
            else
            {
                Config_ComboBox_NetworkType->setCurrentItem(0);
                Config_TextLabel_Channel->show();
                Config_ComboBox_Channel->show();

                if (pBssid->Configuration.DSConfig == 2484000)
                    nChannel = 14;
                else if (pBssid->Configuration.DSConfig >= 2412000 && pBssid->Configuration.DSConfig < 2484000) 
                    nChannel = (int)((pBssid->Configuration.DSConfig - 2412000) / 5000) + 1;

                sprintf(tmp, "%d", nChannel);
                Config_ComboBox_Channel->setCurrentText( tmp );
            }
        }
        pBssid = (PNDIS_WLAN_BSSID_EX)((char *)pBssid + pBssid->Length);
    }
}

void AddProfileDlg::InitChannel()
{
    UCHAR                           aryChan[32] = {0};
    char                            channelString[10];
    int                             nIndex = 0;
    int                             i;

    if (G_enumWirelessMode != PHY_11A)
    {
        switch (G_nCountryRegion)
        {
            case REGSTR_COUNTRYREGION_IC:
                memcpy(&aryChan[nIndex], Ra24Ghz_IC, sizeof(Ra24Ghz_IC));
                nIndex += sizeof(Ra24Ghz_IC);
                break;
            case REGSTR_COUNTRYREGION_ETSI:
                memcpy(&aryChan[nIndex], Ra24Ghz_ESTI, sizeof(Ra24Ghz_ESTI));
                nIndex += sizeof(Ra24Ghz_ESTI);
                break;
            case REGSTR_COUNTRYREGION_SPAIN:
                memcpy(&aryChan[nIndex], Ra24Ghz_SPAIN, sizeof(Ra24Ghz_SPAIN));
                nIndex += sizeof(Ra24Ghz_SPAIN);
                break;
            case REGSTR_COUNTRYREGION_FRANCE:
                memcpy(&aryChan[nIndex], Ra24Ghz_FRANCE, sizeof(Ra24Ghz_FRANCE));
                nIndex += sizeof(Ra24Ghz_FRANCE);
                break;
            case REGSTR_COUNTRYREGION_MKK:
                memcpy(&aryChan[nIndex], Ra24Ghz_MKK, sizeof(Ra24Ghz_MKK));
                nIndex += sizeof(Ra24Ghz_MKK);
                break;
            case REGSTR_COUNTRYREGION_MKK1:
                memcpy(&aryChan[nIndex], Ra24Ghz_MKK1, sizeof(Ra24Ghz_MKK1));
                nIndex += sizeof(Ra24Ghz_MKK1);
                break;
            case REGSTR_COUNTRYREGION_ISRAEL:
                memcpy(&aryChan[nIndex], Ra24Ghz_ISRAEL, sizeof(Ra24Ghz_ISRAEL));
                nIndex += sizeof(Ra24Ghz_ISRAEL);
                break;
            case REGSTR_COUNTRYREGION_FCC:
            default:
                memcpy(&aryChan[nIndex], Ra24Ghz_FCC, sizeof(Ra24Ghz_FCC));
                nIndex += sizeof(Ra24Ghz_FCC);
                break;
        }
    }

    if (G_enumWirelessMode == PHY_11A || G_enumWirelessMode == PHY_11ABG_MIXED)
    {
        memcpy(&aryChan[nIndex], Ra5Ghz_UNII, sizeof(Ra5Ghz_UNII));
        nIndex += sizeof(Ra5Ghz_UNII);
    }

    Config_ComboBox_Channel->clear();
    for (i = 0; i < nIndex; i++)
    {
        sprintf(channelString, "%d", aryChan[i]);
        Config_ComboBox_Channel->insertItem(channelString);
    }
}

bool AddProfileDlg::IsOk()
{
    return (m_IsAddToProfile);
}

void AddProfileDlg::OnCancel()
{
    close();
}

void AddProfileDlg::OnOK()
{
    QListViewItem                       *Item;
    PRT_PROFILE_SETTING                 ptr = NULL;
    PRT_PROFILE_SETTING                 previous=NULL;
    NDIS_802_11_AUTHENTICATION_MODE     AuthMode;
    NDIS_802_11_WEP_STATUS              EncryType;
    QString                             qstrEncryp;
    QString                             qstrAuthen;
    QString                             qstr;
    int                                 keyid;
    bool                                bfound = FALSE;
    char                                msg[255];

    qstrAuthen = Security_ComboBox_AuthType->currentText();
    qstrEncryp = Security_ComboBox_Encrypt->currentText();
    if ((qstrAuthen.compare("WPAPSK") == 0) || (qstrAuthen.compare("WPA-None") == 0))
    { //Use WPAPSK
        qstr = Security_LineEdit_PSK->text();
        if (qstr.length() < 8)
        {
            QMessageBox::warning(this, "Warning", "Invalid WPA Pre-Shared key. WPAPSK used field should use more than 8 characters.");
            TabWidget_Config->setCurrentPage(1);
            Security_LineEdit_PSK->setFocus();
            return;
        }
    }
    else if (qstrEncryp.compare("WEP") == 0)
    {
        keyid = Security_ButtonGroup_Key->id(Security_ButtonGroup_Key->selected());
        qstr = Security_GetWepKeyString(keyid);
        if (Security_GetWepKeyType(keyid) == 0)
        {//Hex
            if ((qstr.length() != 10) && (qstr.length() != 26))
            {
                sprintf(msg, "Invalid WEP KEY length. Key should be 10 or 26 hex digits!");
                QMessageBox::warning(this, "Warning", msg);
                TabWidget_Config->setCurrentPage(1);
                switch (keyid)
                {
                case 0:
                    Security_LineEdit_Key1Hex->setFocus();
                    break;
                case 1:
                    Security_LineEdit_Key2Hex->setFocus();
                    break;
                case 2:
                    Security_LineEdit_Key3Hex->setFocus();
                    break;
                case 3:
                    Security_LineEdit_Key4Hex->setFocus();
                    break;
                }
                return;
            }
        }
        else
        {//Ascii
            if ((qstr.length() != 5) && (qstr.length() != 13))
            {
                sprintf(msg, "Invalid WEP KEY length. Key should be 5 or 13 ascii characters!");
                QMessageBox::warning(this, "Warning", msg);
                TabWidget_Config->setCurrentPage(1);
                switch (keyid)
                {
                case 0:
                    Security_LineEdit_Key1Ascii->setFocus();
                    break;
                case 1:
                    Security_LineEdit_Key2Ascii->setFocus();
                    break;
                case 2:
                    Security_LineEdit_Key3Ascii->setFocus();
                    break;
                case 3:
                    Security_LineEdit_Key4Ascii->setFocus();
                    break;
                }
                return;
            }
        }
    }

    qstr = LineEdit_ProfileName->text();
    if (qstr.isEmpty())
    {
        QMessageBox::warning(this, "Warning", "Profile name can't be empty!");
        return;
    }

    qstr = ComboBox_SSID->currentText();
    if ( (Config_ComboBox_NetworkType->currentItem() == 0) && qstr.isEmpty())
    {
        QMessageBox::warning(this, "Warning", "Ad Hoc mode must input SSID!");
        return;
    }

    qstr = LineEdit_ProfileName->text();
    if (m_type == PROFILE_ADD)
    {
        ptr = m_pProfileSetting;
        if (!m_bIsProfileNull)
        {
            while (ptr != NULL)
            {
                if (qstr.compare((const char *)ptr->Profile) == 0)
                {
                    bfound = TRUE;
                    break;
                }
                previous = ptr;
                ptr=ptr->Next;
            }

            if (!bfound)
            {
                m_pEditProfileSetting = (PRT_PROFILE_SETTING) malloc(sizeof(RT_PROFILE_SETTING));

                memset(m_pEditProfileSetting, 0x00, sizeof(RT_PROFILE_SETTING));
                previous->Next = m_pEditProfileSetting;
                m_pEditProfileSetting->Next = NULL;
                strcpy((char *)m_pEditProfileSetting->Profile, qstr.data());
            }
            else
            {
                QMessageBox::warning(this, "Warning", "Duplicate profile name.!");
                return;
            }
        }
        else
        {
            m_pEditProfileSetting = m_pProfileSetting;
            strcpy((char *)m_pEditProfileSetting->Profile, qstr.data());
            m_pEditProfileSetting->Next = NULL;
        }
    }
    else
    { //Edit Profile, check does it Duplicate profile name?
        ptr = m_pProfileSetting;
        while (ptr != NULL)
        {
            if( (qstr.compare((const char *)ptr->Profile) == 0) && (ptr != m_pEditProfileSetting))
            {
                bfound = TRUE;
                break;
            }
            previous = ptr;
            ptr=ptr->Next;
        }
    }

    if (m_type == PROFILE_ADD)
    {
        Item = new QListViewItem(m_ProfileListView, 0);
        qstr = LineEdit_ProfileName->text();
        Item->setText(0, qstr);
        Item->setPixmap(0, uic_load_pixmap_AddProfileDlg("uncheck16.xpm"));
    }
    else
    {
        Item = m_ProfileListView->findItem((const char *) m_pEditProfileSetting->Profile, 0);
        strcpy((char *)m_pEditProfileSetting->Profile, qstr.data());
        Item->setText(0, qstr);
    }

    //SSID
    qstr = ComboBox_SSID->currentText();
    m_pEditProfileSetting ->SsidLen = qstr.length() <= 32 ? qstr.length() : 32;
    memcpy((char *)m_pEditProfileSetting->SSID, qstr.data(), m_pEditProfileSetting->SsidLen);
    m_pEditProfileSetting->SSID[m_pEditProfileSetting->SsidLen] = 0x00;
    Item->setText(1, (char *)m_pEditProfileSetting->SSID);

    //Channel
    if(Config_GetNetworkType() == Ndis802_11IBSS)
    {
        qstr = Config_ComboBox_Channel->currentText();
        m_pEditProfileSetting->Channel = qstr.toUInt();
    }
    else
        m_pEditProfileSetting->Channel = 0;  // 0: Auto.

    Item->setText(2, (Config_ComboBox_NetworkType->currentItem()==0 ? Config_ComboBox_Channel->currentText():"Auto"));

    //Authentication
    AuthMode = Security_GetAuthticationMode();
    if (AuthMode == Ndis802_11AuthModeOpen)
        Item->setText(3, "OPEN");
    else if (AuthMode == Ndis802_11AuthModeShared)
        Item->setText(3, "SHARED");
    else if (AuthMode == Ndis802_11AuthModeWPAPSK)
        Item->setText(3, "WPAPSK");
    else if (AuthMode == Ndis802_11AuthModeWPANone)
        Item->setText(3, "WPA-None");
    else
        Item->setText(3, "Unknown");

    //Encryption
    EncryType = Security_GetEncryptType();
    if (EncryType == Ndis802_11WEPEnabled)
        Item->setText(4, "WEP");
    else if (EncryType == Ndis802_11WEPDisabled)
        Item->setText(4, "NONE");
    else if (EncryType == Ndis802_11Encryption2Enabled)
        Item->setText(4, "TKIP");
    else if (EncryType == Ndis802_11Encryption3Enabled)
        Item->setText(4, "AES");

    //NetworkType
    m_pEditProfileSetting->NetworkType = Config_GetNetworkType();
    Item->setText(5, strAryNetworkType[Config_ComboBox_NetworkType->currentItem()]);

    //Fragment
    m_pEditProfileSetting->Fragment = Config_SpinBox_Fragment->value();
    //RTS
    m_pEditProfileSetting->RTS = Config_SpinBox_RTS->value();
    //PSmode;
    switch ( Config_ButtonGroup_PowerSaveing->id(Config_ButtonGroup_PowerSaveing->selected()) )
    {
    case 1:
        m_pEditProfileSetting->PSmode = Ndis802_11PowerModeMAX_PSP;
        break;
    case 0:
    default:
        m_pEditProfileSetting->PSmode = Ndis802_11PowerModeCAM;
        break;
    }

    //PreamType
    if (Config_ComboBox_Preamble->currentItem() == 0)
    { //Auto
        m_pEditProfileSetting->PreamType = Rt802_11PreambleAuto;
    }
    else
    { //Long
        m_pEditProfileSetting->PreamType = Rt802_11PreambleLong;
    }

    //RTS Checkbox
    m_pEditProfileSetting->RTSCheck = Config_CheckBox_RTS->isChecked();

    //Fragment Checkbox
    m_pEditProfileSetting->FragmentCheck = Config_CheckBox_Fragment->isChecked();
    m_ProfileListView->setCurrentItem(Item);

    m_pEditProfileSetting->Authentication = Security_GetAuthticationMode();
    m_pEditProfileSetting->Encryption = Security_GetEncryptType();
    m_pEditProfileSetting->KeyDefaultId = Security_GetDefaultKeyId();
    m_pEditProfileSetting->Key1Type = Security_GetWepKeyType(0);
    m_pEditProfileSetting->Key2Type = Security_GetWepKeyType(1);
    m_pEditProfileSetting->Key3Type = Security_GetWepKeyType(2);
    m_pEditProfileSetting->Key4Type = Security_GetWepKeyType(3);
    strcpy(m_pEditProfileSetting->Key1, Security_GetWepKeyString(0));
    strcpy(m_pEditProfileSetting->Key2, Security_GetWepKeyString(1));
    strcpy(m_pEditProfileSetting->Key3, Security_GetWepKeyString(2));
    strcpy(m_pEditProfileSetting->Key4, Security_GetWepKeyString(3));
    strcpy(m_pEditProfileSetting->WpaPsk, Security_GetPSKString());

    m_IsAddToProfile = TRUE;
    close();
}

void AddProfileDlg::Config_OnSelectSSID(int id)
{
    PNDIS_WLAN_BSSID_EX                 pBssid;
    NDIS_802_11_AUTHENTICATION_MODE     AuthenticationMode = Ndis802_11AuthModeOpen;
    NDIS_802_11_ENCRYPTION_STATUS       EncryptType = Ndis802_11WEPDisabled;
    QString                             qStrSsid;
    char                                strSSID[NDIS_802_11_LENGTH_SSID + 1];
    ULONG                               i = 0;
    char                                tmp[40];
    int                                 nChannel = 1;

    if (id == 0)
        return;

    pBssid = m_pBssidList->Bssid;
    for(i = 0; i < (id-1); i++)
    {
        if (i >= m_pBssidList->NumberOfItems)
            break;
        pBssid = (PNDIS_WLAN_BSSID_EX)((char *)pBssid + pBssid->Length);
    }

    memset(strSSID, 0x00, NDIS_802_11_LENGTH_SSID + 1);
    memcpy(strSSID, pBssid->Ssid.Ssid, pBssid->Ssid.SsidLength);

    if (pBssid->Privacy)
        EncryptType = Ndis802_11WEPEnabled;
    else
        EncryptType = Ndis802_11WEPDisabled;

    if ((pBssid->Length > sizeof(NDIS_WLAN_BSSID)) && (pBssid->IELength > sizeof(NDIS_802_11_FIXED_IEs)))
    {
        ULONG lIELoc = 0;
        PNDIS_802_11_FIXED_IEs pFixIE = (PNDIS_802_11_FIXED_IEs)pBssid->IEs;
        PNDIS_802_11_VARIABLE_IEs pVarIE = (PNDIS_802_11_VARIABLE_IEs)((char*)pFixIE + sizeof(NDIS_802_11_FIXED_IEs));
        lIELoc += sizeof(NDIS_802_11_FIXED_IEs);
        while (pBssid->IELength > (lIELoc + sizeof(NDIS_802_11_VARIABLE_IEs)))
        {
            if ((pVarIE->ElementID == 221) && (pVarIE->Length >= 16))
            {
                ULONG* pOUI = (ULONG*)((char*)pVarIE + 2);
                if(*pOUI != WPA_OUI_TYPE)
                    break;
                ULONG* plGroupKey; 
                WORD* pdPairKeyCount;
                ULONG* plAuthenKey; 
                WORD* pdAuthenKeyCount;
                plGroupKey = (ULONG*)((char*)pVarIE + 8);

                ULONG lGroupKey = *plGroupKey & 0x00ffffff;
                if (lGroupKey == WPA_OUI)
                {
                    lGroupKey = (*plGroupKey & 0xff000000) >> 0x18;
                    if (lGroupKey == 2)
                        EncryptType = Ndis802_11Encryption2Enabled;
                    else if (lGroupKey == 3)
                        EncryptType = Ndis802_11Encryption3Enabled;
                    else if (lGroupKey == 4)
                        EncryptType = Ndis802_11Encryption3Enabled;
                }
                else
                    break;

                pdPairKeyCount = (WORD*)((char*)plGroupKey + 4);
                pdAuthenKeyCount = (WORD*)((char*)pdPairKeyCount + 2 + 4 * (*pdPairKeyCount));
                if (*pdAuthenKeyCount > 0)
                {
                    plAuthenKey = (ULONG*)((char*)pdAuthenKeyCount + 2);

                    ULONG lAuthenKey = *plAuthenKey & 0x00ffffff;
                    if (lAuthenKey == WPA_OUI)
                    {
                        lAuthenKey = (*plAuthenKey & 0xff000000) >> 0x18;
                        if (lAuthenKey == 1)
                            AuthenticationMode = Ndis802_11AuthModeWPA;
                        else if (lAuthenKey == 2)
                        {
                            if (pBssid->InfrastructureMode)
                                AuthenticationMode = Ndis802_11AuthModeWPAPSK;
                            else 
                                AuthenticationMode = Ndis802_11AuthModeWPANone;
                        }
                    }
                }
                break;
            }
            lIELoc += pVarIE->Length;
            pVarIE = (PNDIS_802_11_VARIABLE_IEs)((char*)pVarIE + pVarIE->Length);
            
            if(pVarIE->Length <= 0)
                break;
        }
    }

    Security_SetAuthModeAndEncryType(pBssid->InfrastructureMode, AuthenticationMode, EncryptType);

    if (pBssid->InfrastructureMode == Ndis802_11IBSS)
    {
        if (pBssid->Configuration.DSConfig == 2484000)
            nChannel = 14;
        else if (pBssid->Configuration.DSConfig >= 2412000 && pBssid->Configuration.DSConfig < 2484000) 
            nChannel = (int)((pBssid->Configuration.DSConfig - 2412000) / 5000) + 1;

        sprintf(tmp, "%d", nChannel);
        Config_ComboBox_Channel->setCurrentText( tmp );
    }
}

void AddProfileDlg::Config_OnSelectNetworkType(int index)
{
    int                                 current;

    current = Security_ComboBox_AuthType->currentItem();
    if (index == 0)
    {//Ad-hoc mode.
        Security_ComboBox_AuthType->clear();
        Security_ComboBox_AuthType->insertItem("OPEN");
        Security_ComboBox_AuthType->insertItem("SHARED");
        Security_ComboBox_AuthType->insertItem("WPA-None");

        Config_TextLabel_Channel->show();
        Config_ComboBox_Channel->show();

        Config_RadioButton_CAM->setEnabled( FALSE );
        Config_RadioButton_PSMode->setEnabled( FALSE );

        Config_ComboBox_Preamble->setEnabled( TRUE );
    }
    else
    {//Infra
        Security_ComboBox_AuthType->clear();
        Security_ComboBox_AuthType->insertItem("OPEN");
        Security_ComboBox_AuthType->insertItem("SHARED");
        Security_ComboBox_AuthType->insertItem("WPAPSK");

        Config_TextLabel_Channel->hide();
        Config_ComboBox_Channel->hide();

        Config_RadioButton_CAM->setEnabled( TRUE );
        Config_RadioButton_PSMode->setEnabled( TRUE );

        Config_ComboBox_Preamble->setEnabled( FALSE );
    }
    Security_ComboBox_AuthType->setCurrentItem( current );
}

void AddProfileDlg::Config_OnEnableRTS(bool flag)
{
    if (flag)
    {
        Config_Slider_RTS->setEnabled( TRUE );
        Config_SpinBox_RTS->setEnabled( TRUE );
    }
    else
    {
        Config_Slider_RTS->setEnabled( FALSE );
        Config_Slider_RTS->setValue( 2312 );
        Config_SpinBox_RTS->setEnabled( FALSE );
        Config_SpinBox_RTS->setValue( 2312 );
    }

}

void AddProfileDlg::Config_OnEnableFragment(bool flag)
{
    if (flag)
    {
        Config_Slider_Fragment->setEnabled( TRUE );
        Config_SpinBox_Fragment->setEnabled( TRUE );
    }
    else
    {
        Config_Slider_Fragment->setEnabled( FALSE );
        Config_Slider_Fragment->setValue( 2312 );
        Config_SpinBox_Fragment->setEnabled( FALSE );
        Config_SpinBox_Fragment->setValue( 2312 );
    }
}

void AddProfileDlg::Config_OnRTSSliderChanged(int value)
{
    Config_SpinBox_RTS->setValue( value );
}

void AddProfileDlg::Config_OnFragmentSliderChanged(int value)
{
    Config_SpinBox_Fragment->setValue( value );
}

void AddProfileDlg::Config_OnRTSSpinBoxChanged(int value)
{
    Config_Slider_RTS->setValue( value );
}

void AddProfileDlg::Config_OnFragmentSpinBoxChanged(int value)
{
    Config_Slider_Fragment->setValue( value );
}

void AddProfileDlg::Config_SetCurrentSSID(QString text)
{
    ComboBox_SSID->setEditText(text);
}

NDIS_802_11_NETWORK_INFRASTRUCTURE AddProfileDlg::Config_GetNetworkType()
{
    NDIS_802_11_NETWORK_INFRASTRUCTURE  Type = Ndis802_11Infrastructure;

    if (Config_ComboBox_NetworkType->currentItem() == 0)
        Type = Ndis802_11IBSS;
    else if (Config_ComboBox_NetworkType->currentItem() == 1)
        Type = Ndis802_11Infrastructure;

    return (Type);
}

void AddProfileDlg::Security_OnSelectAuthenType(int id)
{
    QString                             textAuthen;

    textAuthen = Security_ComboBox_AuthType->text(id);
    if ((textAuthen.compare("OPEN") == 0) || (textAuthen.compare("SHARED") == 0))
    {
        Security_LineEdit_PSK->setEnabled( FALSE );

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("NONE");
        Security_ComboBox_Encrypt->insertItem("WEP");
        Security_ComboBox_Encrypt->setCurrentItem( 1 );

        Security_RadioButton_Key1->setEnabled( TRUE );
        Security_RadioButton_Key2->setEnabled( TRUE );
        Security_RadioButton_Key3->setEnabled( TRUE );
        Security_RadioButton_Key4->setEnabled( TRUE );

        Security_ComboBox_KeyType1->setEnabled( TRUE );
        Security_ComboBox_KeyType2->setEnabled( TRUE );
        Security_ComboBox_KeyType3->setEnabled( TRUE );
        Security_ComboBox_KeyType4->setEnabled( TRUE );

        Security_LineEdit_Key1Hex->setEnabled( TRUE );
        Security_LineEdit_Key1Ascii->setEnabled( TRUE );
        Security_LineEdit_Key2Hex->setEnabled( TRUE );
        Security_LineEdit_Key2Ascii->setEnabled( TRUE );
        Security_LineEdit_Key3Hex->setEnabled( TRUE );
        Security_LineEdit_Key3Ascii->setEnabled( TRUE );
        Security_LineEdit_Key4Hex->setEnabled( TRUE );
        Security_LineEdit_Key4Ascii->setEnabled( TRUE );

    } 
    else if ((textAuthen.compare("WPAPSK") == 0) || (textAuthen.compare("WPA-None") == 0))
    {
        Security_LineEdit_PSK->setEnabled( TRUE );

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("TKIP");
        Security_ComboBox_Encrypt->insertItem("AES");
        Security_ComboBox_Encrypt->setCurrentItem( 0 );

        Security_RadioButton_Key1->setEnabled( FALSE );
        Security_RadioButton_Key2->setEnabled( FALSE );
        Security_RadioButton_Key3->setEnabled( FALSE );
        Security_RadioButton_Key4->setEnabled( FALSE );

        Security_ComboBox_KeyType1->setEnabled( FALSE );
        Security_ComboBox_KeyType2->setEnabled( FALSE );
        Security_ComboBox_KeyType3->setEnabled( FALSE );
        Security_ComboBox_KeyType4->setEnabled( FALSE );

        Security_LineEdit_Key1Hex->setEnabled( FALSE );
        Security_LineEdit_Key1Ascii->setEnabled( FALSE );
        Security_LineEdit_Key2Hex->setEnabled( FALSE );
        Security_LineEdit_Key2Ascii->setEnabled( FALSE );
        Security_LineEdit_Key3Hex->setEnabled( FALSE );
        Security_LineEdit_Key3Ascii->setEnabled( FALSE );
        Security_LineEdit_Key4Hex->setEnabled( FALSE );
        Security_LineEdit_Key4Ascii->setEnabled( FALSE );
    }
}

void AddProfileDlg::Security_OnSelectEncryptType(int id)
{
    QString                             textAuthen;
    QString                             textEncry;

    textEncry = Security_ComboBox_Encrypt->text(id);
    textAuthen = Security_ComboBox_AuthType->currentText();
    if ((textAuthen.compare("OPEN") == 0) || (textAuthen.compare("SHARED") == 0))
    {
        if (textEncry.compare("NONE") == 0)
        {
            Security_RadioButton_Key1->setEnabled( FALSE );
            Security_RadioButton_Key2->setEnabled( FALSE );
            Security_RadioButton_Key3->setEnabled( FALSE );
            Security_RadioButton_Key4->setEnabled( FALSE );

            Security_ComboBox_KeyType1->setEnabled( FALSE );
            Security_ComboBox_KeyType2->setEnabled( FALSE );
            Security_ComboBox_KeyType3->setEnabled( FALSE );
            Security_ComboBox_KeyType4->setEnabled( FALSE );

            Security_LineEdit_Key1Hex->setEnabled( FALSE );
            Security_LineEdit_Key1Ascii->setEnabled( FALSE );
            Security_LineEdit_Key2Hex->setEnabled( FALSE );
            Security_LineEdit_Key2Ascii->setEnabled( FALSE );
            Security_LineEdit_Key3Hex->setEnabled( FALSE );
            Security_LineEdit_Key3Ascii->setEnabled( FALSE );
            Security_LineEdit_Key4Hex->setEnabled( FALSE );
            Security_LineEdit_Key4Ascii->setEnabled( FALSE );

            Security_ComboBox_AuthType->setCurrentItem(0);
        }
        else
        {
            Security_RadioButton_Key1->setEnabled( TRUE );
            Security_RadioButton_Key2->setEnabled( TRUE );
            Security_RadioButton_Key3->setEnabled( TRUE );
            Security_RadioButton_Key4->setEnabled( TRUE );

            Security_ComboBox_KeyType1->setEnabled( TRUE );
            Security_ComboBox_KeyType2->setEnabled( TRUE );
            Security_ComboBox_KeyType3->setEnabled( TRUE );
            Security_ComboBox_KeyType4->setEnabled( TRUE );

            Security_LineEdit_Key1Hex->setEnabled( TRUE );
            Security_LineEdit_Key1Ascii->setEnabled( TRUE );
            Security_LineEdit_Key2Hex->setEnabled( TRUE );
            Security_LineEdit_Key2Ascii->setEnabled( TRUE );
            Security_LineEdit_Key3Hex->setEnabled( TRUE );
            Security_LineEdit_Key3Ascii->setEnabled( TRUE );
            Security_LineEdit_Key4Hex->setEnabled( TRUE );
            Security_LineEdit_Key4Ascii->setEnabled( TRUE );
        }
    }
}

void AddProfileDlg::Security_OnSelectKey1Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key1Hex->show();
        Security_LineEdit_Key1Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key1Hex->hide();
        Security_LineEdit_Key1Ascii->show();
    }
}

void AddProfileDlg::Security_OnSelectKey2Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key2Hex->show();
        Security_LineEdit_Key2Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key2Hex->hide();
        Security_LineEdit_Key2Ascii->show();
    }
}

void AddProfileDlg::Security_OnSelectKey3Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key3Hex->show();
        Security_LineEdit_Key3Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key3Hex->hide();
        Security_LineEdit_Key3Ascii->show();
    }
}

void AddProfileDlg::Security_OnSelectKey4Type(int id)
{
    if (id==0)
    {//hex
        Security_LineEdit_Key4Hex->show();
        Security_LineEdit_Key4Ascii->hide();
    }
    else
    {//Ascii
        Security_LineEdit_Key4Hex->hide();
        Security_LineEdit_Key4Ascii->show();
    }
}

int AddProfileDlg::Security_GetWepKeyType(int index)
{
    int                                 num;

    switch (index)
    {
    case 1:
        num = Security_ComboBox_KeyType2->currentItem();
        break;
    case 2:
        num = Security_ComboBox_KeyType3->currentItem();
        break;
    case 3:
        num = Security_ComboBox_KeyType4->currentItem();
        break;
    case 0:
    default:
        num = Security_ComboBox_KeyType1->currentItem();
        break;
    }

    return num;
}

QString AddProfileDlg::Security_GetWepKeyString(int index)
{
    switch (index)
    {
    case 1:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key2Hex->text());
        else
            return (Security_LineEdit_Key2Ascii->text());
        break;
    case 2:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key3Hex->text());
        else
            return (Security_LineEdit_Key3Ascii->text());
        break;
    case 3:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key4Hex->text());
        else
            return (Security_LineEdit_Key4Ascii->text());
        break;
    case 0:
    default:
        if ( Security_GetWepKeyType(index) == 0) //Hex
            return (Security_LineEdit_Key1Hex->text());
        else
            return (Security_LineEdit_Key1Ascii->text());
        break;
    }
}

QString AddProfileDlg::Security_GetPSKString()
{
    return (Security_LineEdit_PSK->text());
}

NDIS_802_11_AUTHENTICATION_MODE AddProfileDlg::Security_GetAuthticationMode()
{
    QString                             qstr;
    NDIS_802_11_AUTHENTICATION_MODE     mode = Ndis802_11AuthModeOpen;

    qstr = Security_ComboBox_AuthType->currentText();
    if (qstr.compare("OPEN") == 0)
        mode = Ndis802_11AuthModeOpen;
    else if (qstr.compare("SHARED") == 0)
        mode = Ndis802_11AuthModeShared;
    else if (qstr.compare("WPAPSK") == 0)
        mode = Ndis802_11AuthModeWPAPSK;
    else if (qstr.compare("WPA-None") == 0)
        mode = Ndis802_11AuthModeWPANone;

    return (mode);
}

NDIS_802_11_ENCRYPTION_STATUS AddProfileDlg::Security_GetEncryptType()
{
    QString                             qstr;
    NDIS_802_11_ENCRYPTION_STATUS       type = Ndis802_11WEPDisabled;

    qstr = Security_ComboBox_Encrypt->currentText();
    if (qstr.compare("NONE") == 0)
        type = Ndis802_11WEPDisabled;
    else if (qstr.compare("WEP") == 0)
        type = Ndis802_11WEPEnabled;
    else if (qstr.compare("TKIP") == 0)
        type = Ndis802_11Encryption2Enabled;
    else if (qstr.compare("AES") == 0)
        type = Ndis802_11Encryption3Enabled;

    return (type);
}

void AddProfileDlg::Security_SetAuthModeAndEncryType(NDIS_802_11_NETWORK_INFRASTRUCTURE NetworkType, NDIS_802_11_AUTHENTICATION_MODE mode, NDIS_802_11_ENCRYPTION_STATUS type)
{
    if (NetworkType == Ndis802_11IBSS)
    {
        Security_ComboBox_AuthType->clear();
        Security_ComboBox_AuthType->insertItem("OPEN");
        Security_ComboBox_AuthType->insertItem("SHARED");
        Security_ComboBox_AuthType->insertItem("WPA-None");

        Config_TextLabel_Channel->show();
        Config_ComboBox_Channel->show();

        Config_RadioButton_CAM->setEnabled( FALSE );
        Config_RadioButton_PSMode->setEnabled( FALSE );

        Config_ComboBox_Preamble->setEnabled( TRUE );

        Config_ComboBox_NetworkType->setCurrentItem( 0 );  //Adhoc
    }
    else
    {
        Security_ComboBox_AuthType->clear();
        Security_ComboBox_AuthType->insertItem("OPEN");
        Security_ComboBox_AuthType->insertItem("SHARED");
        Security_ComboBox_AuthType->insertItem("WPAPSK");

        Config_TextLabel_Channel->hide();
        Config_ComboBox_Channel->hide();

        Config_RadioButton_CAM->setEnabled( TRUE );
        Config_RadioButton_PSMode->setEnabled( TRUE );

        Config_ComboBox_Preamble->setEnabled( FALSE );

        Config_ComboBox_NetworkType->setCurrentItem( 1 );  //Infrastructure
    }

    if (mode == Ndis802_11AuthModeOpen)
        Security_ComboBox_AuthType->setCurrentItem( 0 );  //OPEN
    else if (mode == Ndis802_11AuthModeShared) 
        Security_ComboBox_AuthType->setCurrentItem( 1 );  //SHARED
    else if ((mode == Ndis802_11AuthModeWPAPSK) || (mode == Ndis802_11AuthModeWPANone))
        Security_ComboBox_AuthType->setCurrentItem( 2 );  //WPAPSK or WPA-None

    if ((mode == Ndis802_11AuthModeOpen) || (mode == Ndis802_11AuthModeShared))
    {
        Security_LineEdit_PSK->setEnabled( FALSE );

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("NONE");
        Security_ComboBox_Encrypt->insertItem("WEP");
        
        if (type == Ndis802_11WEPEnabled)
        {
            Security_ComboBox_Encrypt->setCurrentItem( 1 );

            Security_RadioButton_Key1->setEnabled( TRUE );
            Security_RadioButton_Key2->setEnabled( TRUE );
            Security_RadioButton_Key3->setEnabled( TRUE );
            Security_RadioButton_Key4->setEnabled( TRUE );

            Security_ComboBox_KeyType1->setEnabled( TRUE );
            Security_ComboBox_KeyType2->setEnabled( TRUE );
            Security_ComboBox_KeyType3->setEnabled( TRUE );
            Security_ComboBox_KeyType4->setEnabled( TRUE );

            Security_LineEdit_Key1Hex->setEnabled( TRUE );
            Security_LineEdit_Key1Ascii->setEnabled( TRUE );
            Security_LineEdit_Key2Hex->setEnabled( TRUE );
            Security_LineEdit_Key2Ascii->setEnabled( TRUE );
            Security_LineEdit_Key3Hex->setEnabled( TRUE );
            Security_LineEdit_Key3Ascii->setEnabled( TRUE );
            Security_LineEdit_Key4Hex->setEnabled( TRUE );
            Security_LineEdit_Key4Ascii->setEnabled( TRUE );
        }
        else
        {
            Security_ComboBox_Encrypt->setCurrentItem( 0 );

            Security_RadioButton_Key1->setEnabled( FALSE );
            Security_RadioButton_Key2->setEnabled( FALSE );
            Security_RadioButton_Key3->setEnabled( FALSE );
            Security_RadioButton_Key4->setEnabled( FALSE );

            Security_ComboBox_KeyType1->setEnabled( FALSE );
            Security_ComboBox_KeyType2->setEnabled( FALSE );
            Security_ComboBox_KeyType3->setEnabled( FALSE );
            Security_ComboBox_KeyType4->setEnabled( FALSE );

            Security_LineEdit_Key1Hex->setEnabled( FALSE );
            Security_LineEdit_Key1Ascii->setEnabled( FALSE );
            Security_LineEdit_Key2Hex->setEnabled( FALSE );
            Security_LineEdit_Key2Ascii->setEnabled( FALSE );
            Security_LineEdit_Key3Hex->setEnabled( FALSE );
            Security_LineEdit_Key3Ascii->setEnabled( FALSE );
            Security_LineEdit_Key4Hex->setEnabled( FALSE );
            Security_LineEdit_Key4Ascii->setEnabled( FALSE );
        }
    }
    else if (mode == Ndis802_11AuthModeWPAPSK)
    {
        Security_LineEdit_PSK->setEnabled( TRUE );

        Security_ComboBox_Encrypt->clear();
        Security_ComboBox_Encrypt->insertItem("TKIP");
        Security_ComboBox_Encrypt->insertItem("AES");

        if (type == Ndis802_11Encryption2Enabled )
            Security_ComboBox_Encrypt->setCurrentItem( 0 );
        else if(type == Ndis802_11Encryption3Enabled)
            Security_ComboBox_Encrypt->setCurrentItem( 1 );

        Security_RadioButton_Key1->setEnabled( FALSE );
        Security_RadioButton_Key2->setEnabled( FALSE );
        Security_RadioButton_Key3->setEnabled( FALSE );
        Security_RadioButton_Key4->setEnabled( FALSE );

        Security_ComboBox_KeyType1->setEnabled( FALSE );
        Security_ComboBox_KeyType2->setEnabled( FALSE );
        Security_ComboBox_KeyType3->setEnabled( FALSE );
        Security_ComboBox_KeyType4->setEnabled( FALSE );

        Security_LineEdit_Key1Hex->setEnabled( FALSE );
        Security_LineEdit_Key1Ascii->setEnabled( FALSE );
        Security_LineEdit_Key2Hex->setEnabled( FALSE );
        Security_LineEdit_Key2Ascii->setEnabled( FALSE );
        Security_LineEdit_Key3Hex->setEnabled( FALSE );
        Security_LineEdit_Key3Ascii->setEnabled( FALSE );
        Security_LineEdit_Key4Hex->setEnabled( FALSE );
        Security_LineEdit_Key4Ascii->setEnabled( FALSE );
    }
}

int AddProfileDlg::Security_GetDefaultKeyId()
{
    int                                 keyId;

    keyId = Security_ButtonGroup_Key->id(Security_ButtonGroup_Key->selected());

    return (keyId);
}

void AddProfileDlg::Security_SetDefaultKeyId(int keyId)
{
    Security_ButtonGroup_Key->setButton(keyId);
}

void AddProfileDlg::Security_SetKeyTypeAndKeyString(int keyIndex, int keyType, char *keyString)
{
    switch (keyIndex)
    {
    case 0:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType1->setCurrentItem( 0 );
            Security_LineEdit_Key1Hex->setText(keyString);
            Security_LineEdit_Key1Hex->show();
            Security_LineEdit_Key1Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType1->setCurrentItem( 1 );
            Security_LineEdit_Key1Ascii->setText(keyString);
            Security_LineEdit_Key1Ascii->show();
            Security_LineEdit_Key1Hex->hide();
        }
        break;
    case 1:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType2->setCurrentItem( 0 );
            Security_LineEdit_Key2Hex->setText(keyString);
            Security_LineEdit_Key2Hex->show();
            Security_LineEdit_Key2Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType2->setCurrentItem( 1 );
            Security_LineEdit_Key2Ascii->setText(keyString);
            Security_LineEdit_Key2Ascii->show();
            Security_LineEdit_Key2Hex->hide();
        }
        break;
    case 2:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType3->setCurrentItem( 0 );
            Security_LineEdit_Key3Hex->setText(keyString);
            Security_LineEdit_Key3Hex->show();
            Security_LineEdit_Key3Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType3->setCurrentItem( 1 );
            Security_LineEdit_Key3Ascii->setText(keyString);
            Security_LineEdit_Key3Ascii->show();
            Security_LineEdit_Key3Hex->hide();
        }
        break;
    case 3:
        if (keyType == 0) //Hex
        {
            Security_ComboBox_KeyType4->setCurrentItem( 0 );
            Security_LineEdit_Key4Hex->setText(keyString);
            Security_LineEdit_Key4Hex->show();
            Security_LineEdit_Key4Ascii->hide();
        }
        else
        {
            Security_ComboBox_KeyType4->setCurrentItem( 1 );
            Security_LineEdit_Key4Ascii->setText(keyString);
            Security_LineEdit_Key4Ascii->show();
            Security_LineEdit_Key4Hex->hide();
        }
        break;
    }
}
