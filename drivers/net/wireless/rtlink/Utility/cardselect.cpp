/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

    Module Name:
    cardselect.cpp

    Abstract:
        Implement card select Dialog.

    Revision History:
    Who            When          What
    --------    ----------      ----------------------------------------------
    Paul Wu     01-22-2003      created
    Paul Wu     11-10-2003      Modify for RT2500 ApConfig

*/

#include "cardselect.h"
#include "rt_tool.h"

#include <qapplication.h>
#include <qvariant.h>
#include <qheader.h>
#include <qlistview.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qmessagebox.h>

static QPixmap uic_load_pixmap_CardSelect( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
        return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}
/* 
 *  Constructs a CardSelect which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
CardSelect::CardSelect( int Socket_Id, PRT_DEVICE_ADAPTER prtAdapter,  QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
        setName( "CardSelect" );
    resize( 493, 224 ); 
    setMinimumSize( 493, 224 ); 
    setMaximumSize( 493, 224 ); 
    setCaption("Select Wireless Adapter");

    qApp->setStyle("Windows");

    TextLabel1 = new QLabel(this, "TextLabel1" );
    TextLabel1->setGeometry( QRect( 20, 20, 400, 20 ) ); 
    TextLabel1->setText("Please select a adapter to configure :");

    OkPushButton = new QPushButton( this, "OkPushButton" );
    OkPushButton->setGeometry( QRect( 216, 180, 60, 30 ) ); 
    OkPushButton->setText("&Ok");

    AdapterListView = new QListView( this, "AdapterListView" );
    AdapterListView->addColumn("");
    AdapterListView->addColumn("   Device Name   ");
    AdapterListView->addColumn("   Type   ");
    AdapterListView->addColumn("   MAC Address   ");
    AdapterListView->setGeometry( QRect( 20, 50, 450, 111 ) ); 
    AdapterListView->setSelectionMode( QListView::Single );
    AdapterListView->setHScrollBarMode( QListView::AlwaysOn );
    AdapterListView->setAllColumnsShowFocus( TRUE );

    QListViewItem *Item;
    int first=TRUE;
    char tmp[40];
    int index=1; 

    while(prtAdapter)
    {
        Item = new QListViewItem(AdapterListView, 0);
        sprintf(tmp, " %d ", index++);
        Item -> setText(0, tmp);
        Item -> setPixmap(0, uic_load_pixmap_CardSelect("adapter.xpm"));
        Item -> setText(1, (const char *)prtAdapter->Device_Name);
        Item -> setText(2, "   Wireless Ethernet   ");
        NDIS_802_11_MAC_ADDRESS        CurrentAddress;

        if( OidQueryInformation(OID_802_3_CURRENT_ADDRESS, Socket_Id, prtAdapter->Device_Name, &CurrentAddress, sizeof(CurrentAddress)) >= 0)
        {
            sprintf(tmp, "%02X-%02X-%02X-%02X-%02X-%02X", CurrentAddress[0], CurrentAddress[1],
                        CurrentAddress[2], CurrentAddress[3], CurrentAddress[4], CurrentAddress[5]);
            Item -> setText(3, (const char *)tmp);
        }

        if(first)
        {
            AdapterListView->setSelected(Item, TRUE);
            first=FALSE;
        }
        prtAdapter = prtAdapter->Next;
    }

    // signals and slots connections
    connect( OkPushButton, SIGNAL( clicked() ), this, SLOT( OkPushButton_Clicked() ) );
    connect( AdapterListView, SIGNAL( doubleClicked(QListViewItem*) ), this, SLOT( AdapterListView_doubleClicked() ) );

    bClickOk = FALSE;
}

/*  
 *  Destroys the object and frees any allocated resources
 */
CardSelect::~CardSelect()
{
    // no need to delete child widgets, Qt does it all for us
}

void CardSelect::AdapterListView_doubleClicked()
{
    OkPushButton_Clicked();
}

const char *CardSelect::Get_Device_Name()
{
    return device.ascii();
}

void CardSelect::OkPushButton_Clicked()
{
    QListViewItem *Item;
    QString str;

    Item = AdapterListView->currentItem();
    if(Item == NULL)
    {
        QMessageBox::warning(this, "Warning", "Please select a adapter first.!");
        return ;
    }
    device=Item->text(1);
    bClickOk = TRUE;
    close();
}

bool CardSelect::isClickOk()
{
    return (bClickOk);
}
