/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

    Module Name:
    configapi.h

    Abstract:
        Implement getkeystring / writekeystring like windows API.

    Revision History:
    Who            When          What
    --------    ----------      ----------------------------------------------
    Paul Wu     01-22-2003      created

*/

#ifndef _CONFIGAPI_H
#define _CONFIGAPI_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_CHARS 256          // Maximum characters/line in Configuration File.
#define NEWLINE   "\n"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

int isfound(char *ptr, char *ini_buffer);
char *find_header(char *ini_buffer, char *header_name);
int get_parameter(char *str, int strsize, char *key, char *offset);
char *find_parameter(char *key, char *offset, int *length);
int writekeystring(char *section, char *key, char *str, char *path, char *filename);
int getkeystring(char *section, char *key, char *dest, int strsize, char *path, char *filename);

#endif //_CONFIGAPI_H
