/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology 5th Rd.
 * Science-based Industrial Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved. Ralink's source code is an unpublished work and the
 * use of a copyright notice does not imply otherwise. This source code
 * contains confidential trade secret material of Ralink Tech. Any attemp
 * or participation in deciphering, decoding, reverse engineering or in any
 * way altering the source code is stricitly prohibited, unless the prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

    Module Name:
    countryform.cpp

    Abstract:
        Implement Country Region Select Dialog.

    Revision History:
    Who            When          What
    --------    ----------      ----------------------------------------------
    Paul Wu     01-22-2003      created

*/

#include "countryform.h"
#include "rt_tool.h"

#include <qapplication.h>
#include <qvariant.h>
#include <qbuttongroup.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

static QPixmap uic_load_pixmap_CountryForm( const QString &name )
{
    const QMimeSource *m = QMimeSourceFactory::defaultFactory()->data( name );
    if ( !m )
        return QPixmap();
    QPixmap pix;
    QImageDrag::decode( m, pix );
    return pix;
}
/* 
 *  Constructs a CountryForm which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
CountryForm::CountryForm( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
        setName( "CountryForm" );
    resize( 530, 227 ); 
    setMinimumSize( 530, 227 ); 
    setMaximumSize( 530, 227 ); 
    qApp->setStyle("Windows");

    setCaption("Region Select");

    InfoTextLabel = new QLabel( this, "InfoTextLabel" );
    InfoTextLabel->setGeometry( QRect( 20, 10, 500, 30 ) ); 
    InfoTextLabel->setText("Please select a country region according to your current region.");

    OkPushButton = new QPushButton( this, "OkPushButton" );
    OkPushButton->setGeometry( QRect( 160, 190, 80, 22 ) ); 
    OkPushButton->setText("&Ok");

    CancelPushButton = new QPushButton( this, "CancelPushButton" );
    CancelPushButton->setGeometry( QRect( 290, 190, 80, 22 ) ); 
    CancelPushButton->setText("&Cancel");

    SelectButtonGroup = new QButtonGroup( this, "SelectButtonGroup" );
    SelectButtonGroup->setGeometry( QRect( 20, 40, 490, 131 ) ); 
    SelectButtonGroup->setTitle("Region Select");

    ISRAELRadioButton = new QRadioButton( SelectButtonGroup, "ISRAELRadioButton" );
    ISRAELRadioButton->setGeometry( QRect( 250, 100, 211, 20 ) ); 
    ISRAELRadioButton->setText("ISRAEL (Channel 3 - 9)");
    SelectButtonGroup->insert(ISRAELRadioButton, REGSTR_COUNTRYREGION_ISRAEL);

    ICRadioButton = new QRadioButton( SelectButtonGroup, "ICRadioButton" );
    ICRadioButton->setGeometry( QRect( 20, 50, 191, 21 ) ); 
    ICRadioButton->setText("IC (Canada) (Channel 1 - 11)");
    SelectButtonGroup->insert(ICRadioButton, REGSTR_COUNTRYREGION_IC);

    ETSIRadioButton = new QRadioButton( SelectButtonGroup, "ETSIRadioButton" );
    ETSIRadioButton->setGeometry( QRect( 20, 75, 211, 21 ) ); 
    ETSIRadioButton->setText("ETSI (Channel 1 - 13)");
    SelectButtonGroup->insert(ETSIRadioButton, REGSTR_COUNTRYREGION_ETSI);

    MKKRadioButton = new QRadioButton( SelectButtonGroup, "MKKRadioButton" );
    MKKRadioButton->setGeometry( QRect( 250, 50, 211, 21 ) ); 
    MKKRadioButton->setText("MKK (Channel 14)");
    SelectButtonGroup->insert(MKKRadioButton, REGSTR_COUNTRYREGION_MKK);

    MKK1RadioButton = new QRadioButton( SelectButtonGroup, "MKK1RadioButton" );
    MKK1RadioButton->setGeometry( QRect( 250, 75, 210, 20 ) ); 
    MKK1RadioButton->setText("MKK1 (TELEC) (Channel 1 - 14)");
    SelectButtonGroup->insert(MKK1RadioButton, REGSTR_COUNTRYREGION_MKK1);

    FRANCERadioButton = new QRadioButton( SelectButtonGroup, "FRANCERadioButton" );
    FRANCERadioButton->setGeometry( QRect( 249, 25, 211, 20 ) ); 
    FRANCERadioButton->setText("FRANCE (Channel 10 - 13)");
    SelectButtonGroup->insert(FRANCERadioButton, REGSTR_COUNTRYREGION_FRANCE);

    SPAINRadioButton = new QRadioButton( SelectButtonGroup, "SPAINRadioButton" );
    SPAINRadioButton->setGeometry( QRect( 20, 100, 164, 19 ) ); 
    SPAINRadioButton->setText("SPAIN (Channel 10 - 11)");
    SelectButtonGroup->insert(SPAINRadioButton, REGSTR_COUNTRYREGION_SPAIN);

    FCCRadioButton = new QRadioButton( SelectButtonGroup, "FCCRadioButton" );
    FCCRadioButton->setGeometry( QRect( 20, 25, 201, 21 ) ); 
    FCCRadioButton->setText("FCC (Channel 1 - 11)(Default)");
    FCCRadioButton->setChecked( TRUE );
    SelectButtonGroup->insert( FCCRadioButton, REGSTR_COUNTRYREGION_FCC);

    // signals and slots connections
    connect( CancelPushButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( OkPushButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
CountryForm::~CountryForm()
{
    // no need to delete child widgets, Qt does it all for us
}

int CountryForm::GetRegionID()
{
    return (SelectButtonGroup->id(SelectButtonGroup->selected()));
}
