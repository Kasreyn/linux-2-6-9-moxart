/****************************************************************************
** Form implementation generated from reading ui file 'hiddenssiddlg.ui'
**
** Created: Tue Dec 23 18:23:18 2003
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "hiddenssiddlg.h"

#include <qvariant.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qmessagebox.h>

/* 
 *  Constructs a HiddenSsidDlg which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
HiddenSsidDlg::HiddenSsidDlg( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
        setName( "HiddenSsidDlg" );
    resize( 350, 115 );
    setMinimumSize( 350, 115 );
    setMaximumSize( 350, 115 );
    setCaption("Hidden SSID");

    TextLabel1 = new QLabel( this, "TextLabel1" );
    TextLabel1->setGeometry( QRect( 10, 8, 330, 31 ) ); 
    TextLabel1->setText("This AP use hidden SSID, please manually config SSID");

    LineEdit_SSID = new QLineEdit( this, "LineEdit_SSID" );
    LineEdit_SSID->setGeometry( QRect( 10, 40, 330, 30 ) ); 
    LineEdit_SSID->setMaxLength(32);

    PushButton_OK = new QPushButton( this, "PushButton_OK" );
    PushButton_OK->setGeometry( QRect( 47, 80, 100, 30 ) ); 
    PushButton_OK->setText("&Ok");

    PushButton_Cancel = new QPushButton( this, "PushButton_Cancel" );
    PushButton_Cancel->setGeometry( QRect( 205, 80, 90, 30 ) ); 
    PushButton_Cancel->setText("&Cancel");

    connect( PushButton_OK, SIGNAL( clicked() ), this, SLOT( OnOK() ) );
    connect( PushButton_Cancel, SIGNAL( clicked() ), this, SLOT( OnCancel() ) );

    m_isClickOk = FALSE;
}

/*  
 *  Destroys the object and frees any allocated resources
 */
HiddenSsidDlg::~HiddenSsidDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

QString HiddenSsidDlg::GetSsidString()
{
    return (LineEdit_SSID->text());
}

bool HiddenSsidDlg::IsClickOk()
{
    return (m_isClickOk);
}

void HiddenSsidDlg::OnCancel()
{
    m_isClickOk = FALSE;
    close();
}

void HiddenSsidDlg::OnOK()
{
    QString             qstr;

    qstr = LineEdit_SSID->text();
    if (qstr.isEmpty())
    {
        QMessageBox::warning(this, "Warning", "Please manually config SSID");
        return;
    }

    m_isClickOk = TRUE;
    close();
}
