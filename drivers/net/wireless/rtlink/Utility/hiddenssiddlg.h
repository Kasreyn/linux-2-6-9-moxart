#ifndef HIDDENSSIDDLG_H
#define HIDDENSSIDDLG_H

#include <qvariant.h>
#include <qdialog.h>
class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QLabel;
class QLineEdit;
class QPushButton;

class HiddenSsidDlg : public QDialog
{ 
    Q_OBJECT

public:
    HiddenSsidDlg( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~HiddenSsidDlg();

    QLabel*                     TextLabel1;
    QLineEdit*                  LineEdit_SSID;
    QPushButton*                PushButton_OK;
    QPushButton*                PushButton_Cancel;

    bool                        m_isClickOk;
    QString GetSsidString();
    bool    IsClickOk();
public slots:
    virtual void Do_NotThing() { };
    virtual void OnOK();
    virtual void OnCancel();
};

#endif // HIDDENSSIDDLG_H
