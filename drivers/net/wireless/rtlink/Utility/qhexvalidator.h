/*
 ***************************************************************************
 * Ralink Tech Inc.
 * 4F, No. 2 Technology	5th	Rd.
 * Science-based Industrial	Park
 * Hsin-chu, Taiwan, R.O.C.
 *
 * (c) Copyright 2002, Ralink Technology, Inc.
 *
 * All rights reserved.	Ralink's source	code is	an unpublished work	and	the
 * use of a	copyright notice does not imply	otherwise. This	source code
 * contains	confidential trade secret material of Ralink Tech. Any attemp
 * or participation	in deciphering,	decoding, reverse engineering or in	any
 * way altering	the	source code	is stricitly prohibited, unless	the	prior
 * written consent of Ralink Technology, Inc. is obtained.
 ***************************************************************************

	Module Name:
	qhexvalidator.h

	Abstract:
		Implement hex validator for WEP security.

	Revision History:
	Who			When		  What
	--------	----------	  ----------------------------------------------
	Paul Wu		01-22-2003	  created

*/

#ifndef __QHEXVALIDATOR_H
#define __QHEXVALIDATOR_H

#include <qvalidator.h>

class QHexValidator : public QValidator {

  public:
    QHexValidator ( QWidget * parent, const char * name = 0 );
    virtual ~QHexValidator ();
    virtual State validate ( QString &, int & ) const;
};


#endif

