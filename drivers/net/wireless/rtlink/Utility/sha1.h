#ifndef SHA1_H
#define SHA1_H

#define A_SHA_DIGEST_LEN 20

typedef struct 
{
    unsigned long   H[5];
    unsigned long   W[80];
    int             lenW;
    unsigned long   sizeHi,sizeLo;
} A_SHA_CTX;

void A_SHAInit(A_SHA_CTX *ctx);
void A_SHAUpdate(A_SHA_CTX *ctx, unsigned char *dataIn, int len);
void A_SHAFinal(A_SHA_CTX *ctx, unsigned char hashout[20]);

#endif /* SHA1_H */
